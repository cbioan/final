package msd.service;

import lombok.extern.slf4j.Slf4j;
import msd.config.MsdBackEndApiConfig;
import msd.exception.BackgroundTaskException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class MsdBackEndApi {

    private RestTemplate restTemplate;
    private static final String REST_ENDPOINT = "/camunda";
    private String restCallUrl;

    public MsdBackEndApi(MsdBackEndApiConfig msdBackEndApiConfig, RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.restCallUrl = msdBackEndApiConfig.serviceUrl + REST_ENDPOINT;
    }

    public void timeoutDelegate(String processId) {

        String callUrl = this.restCallUrl + "/timeout/" + processId;
        try {
            restTemplate.exchange(callUrl, HttpMethod.POST, new HttpEntity<>(null), Void.class);
        } catch (Exception e) {
            throwUnexpectedFailure(e);
        }

    }

    public void notFoundDelegate(String processId) {

        String callUrl = this.restCallUrl + "/notfound/" + processId;
        try {
            restTemplate.exchange(callUrl, HttpMethod.POST, new HttpEntity<>(null), Void.class);
        } catch (Exception e) {
            throwUnexpectedFailure(e);
        }

    }

    public void savedToDatabase(String processId) {

        String callUrl = this.restCallUrl + "/service/" + processId;
        try {
            restTemplate.exchange(callUrl, HttpMethod.POST, new HttpEntity<>(null), Void.class);
        } catch (Exception e) {
            throwUnexpectedFailure(e);
        }

    }

    private void throwUnexpectedFailure(Exception e) {
        log.error(e.getMessage());
        throw new BackgroundTaskException(e.getMessage());
    }

}
