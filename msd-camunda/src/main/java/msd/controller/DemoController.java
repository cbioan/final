package msd.controller;

import api.CamundaDemoApi;
import api.domain.ActiveTask;
import api.domain.CompleteTask;
import api.domain.StartProcess;
import msd.exception.EntityNotFoundException;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.exception.NullValueException;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/demo")
public class DemoController implements CamundaDemoApi {

    private static final String TASK_DOES_NOT_EXIST = "The queried task does not exist";
    private static final String PROCESS_DOES_NOT_EXIST = "The process does not exist";

    private final RuntimeService runtimeService;
    private final TaskService taskService;

    public DemoController(RuntimeService runtimeService, TaskService taskService) {
        this.runtimeService = runtimeService;
        this.taskService = taskService;
    }

    @GetMapping
    public StartProcess beginProcess() {

        try {
            ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("msd_demo");
            Task task = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
            return new StartProcess(processInstance.getId(), task.getId());
        } catch (NullPointerException | NullValueException e) {
            throw new EntityNotFoundException(PROCESS_DOES_NOT_EXIST);
        }

    }

    @PostMapping(value = "/productSelect/{taskId}")
    public ActiveTask productSelect(@RequestBody String productSelect, @PathVariable(value = "taskId") String taskId) {

        try {
            Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
            taskService.setVariable(task.getId(), "productSelect", productSelect);
            taskService.complete(task.getId());
            Task activeTask = taskService.createTaskQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
            return new ActiveTask(activeTask.getId());
        } catch (NullPointerException | NullValueException e) {
            throw new EntityNotFoundException(TASK_DOES_NOT_EXIST);
        }
    }

    @PostMapping(value = "/confirmAction/{taskId}/{async}")
    public CompleteTask confirmAction(@PathVariable(value = "taskId") String taskId,
                                      @PathVariable(value = "async") Boolean async) {

        try {
            Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
            if (async) {
                new Thread(() -> taskService.complete(task.getId())).start();
                return new CompleteTask(task.getId(), "Process completed async", true);
            } else {
                taskService.complete(task.getId());
                return new CompleteTask(task.getId(), "Process completed sync", false);
            }

        } catch (NullPointerException | NullValueException e) {
            throw new EntityNotFoundException(TASK_DOES_NOT_EXIST);
        }
    }

}
