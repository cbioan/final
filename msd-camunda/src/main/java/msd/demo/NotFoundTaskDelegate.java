package msd.demo;

import lombok.extern.slf4j.Slf4j;
import msd.service.MsdBackEndApi;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class NotFoundTaskDelegate implements JavaDelegate {

    private final MsdBackEndApi msdBackEndApi;

    public NotFoundTaskDelegate(MsdBackEndApi msdBackEndApi) {
        this.msdBackEndApi = msdBackEndApi;
    }

    @Override
    public void execute(DelegateExecution execution) {
        log.info("NotFoundTaskDelegate");
        msdBackEndApi.notFoundDelegate(execution.getProcessInstanceId());
    }

}