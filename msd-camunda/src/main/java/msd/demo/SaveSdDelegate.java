package msd.demo;

import lombok.extern.slf4j.Slf4j;
import msd.service.MsdBackEndApi;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class SaveSdDelegate implements JavaDelegate {

    private final MsdBackEndApi msdBackEndApi;

    public SaveSdDelegate(MsdBackEndApi msdBackEndApi) {
        this.msdBackEndApi = msdBackEndApi;
    }

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        TimeUnit.SECONDS.sleep(5);
        log.info("SavedSdDelegate");
        msdBackEndApi.savedToDatabase(execution.getProcessInstanceId());
    }

}