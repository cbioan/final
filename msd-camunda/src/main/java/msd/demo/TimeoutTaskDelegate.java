package msd.demo;

import lombok.extern.slf4j.Slf4j;
import msd.service.MsdBackEndApi;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TimeoutTaskDelegate implements JavaDelegate {

    private final MsdBackEndApi msdBackEndApi;

    public TimeoutTaskDelegate(MsdBackEndApi msdBackEndApi) {
        this.msdBackEndApi = msdBackEndApi;
    }

    @Override
    public void execute(DelegateExecution execution) {
        log.info("TimeoutTaskDelegate");
        msdBackEndApi.timeoutDelegate(execution.getProcessInstanceId());
    }

}
