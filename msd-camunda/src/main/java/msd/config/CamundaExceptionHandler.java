package msd.config;

import common.error.ApiError;
import common.error.RestExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import msd.controller.DemoController;
import msd.exception.EntityNotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.*;

@RestControllerAdvice(assignableTypes = DemoController.class)
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class CamundaExceptionHandler extends RestExceptionHandler {

    private ResponseEntity<Object> setLocationException(HttpStatus unauthorized, String message) {
        ApiError apiError = new ApiError(unauthorized);
        apiError.setMessage(message);
        log.error(message);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> handleException(EntityNotFoundException ex) {
        return setLocationException(NOT_FOUND, ex.getMessage());
    }

}
