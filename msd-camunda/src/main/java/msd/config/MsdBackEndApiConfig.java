package msd.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:msdbe.properties")
@Configuration
public class MsdBackEndApiConfig {

    public MsdBackEndApiConfig() {
        //Empty Constructor
    }

    @Value("${msdbe.serviceUrl}")
    public String serviceUrl;

}
