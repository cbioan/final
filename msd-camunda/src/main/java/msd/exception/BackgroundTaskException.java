package msd.exception;

public class BackgroundTaskException extends RuntimeException {
    public BackgroundTaskException() { super(); }
    public BackgroundTaskException(String message) { super(message); }
}
