package msd.controller;

import api.domain.ActiveTask;
import api.domain.StartProcess;
import com.fasterxml.jackson.databind.ObjectMapper;
import msd.config.CamundaExceptionHandler;
import msd.service.MsdBackEndApi;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {DemoController.class, CamundaExceptionHandler.class})
@AutoConfigureMockMvc
@AutoConfigureWebMvc
@EnableAutoConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DemoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RuntimeService runtimeService;

    @Autowired
    private TaskService taskService;

    @MockBean
    private MsdBackEndApi msdBackEndApi;

    @InjectMocks
    private DemoController demoController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private StartProcess returnStartProcessObject() throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        MvcResult result = mockMvc
                .perform(get("/demo"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        return mapper.readValue(result.getResponse().getContentAsString(), StartProcess.class);

    }

    @Test
    public void a_beginProcess_expectSuccess() throws Exception {

        mockMvc
                .perform(get("/demo"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(jsonPath("$.processId").hasJsonPath())
                .andExpect(jsonPath("$.taskId").hasJsonPath())
                .andExpect(jsonPath("$.processId").value(is(not(isEmptyOrNullString()))))
                .andExpect(jsonPath("$.taskId").value(is(not(isEmptyOrNullString()))))
                .andReturn();
    }

    @Test
    public void b_productSelect_expectTaskNotFound() throws Exception {

        StartProcess startProcess = this.returnStartProcessObject();

        mockMvc
                .perform(post("/demo/productSelect/not_found_id_" + startProcess.getTaskId())
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("Internet"))
                .andExpect(status().is(404))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

    }

    @Test
    public void c_productSelect_expectInternetAction() throws Exception {

        StartProcess startProcess = this.returnStartProcessObject();

        mockMvc
                .perform(post("/demo/productSelect/" + startProcess.getTaskId())
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("Internet"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.activeTaskId").hasJsonPath())
                .andExpect(jsonPath("$.activeTaskId").value(is(not(isEmptyOrNullString()))))
                .andReturn();

    }

    @Test
    public void d_productSelect_expectTelephonyAction() throws Exception {

        StartProcess startProcess = this.returnStartProcessObject();

        mockMvc
                .perform(post("/demo/productSelect/" + startProcess.getTaskId())
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("Telephony"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.activeTaskId").hasJsonPath())
                .andExpect(jsonPath("$.activeTaskId").value(is(not(isEmptyOrNullString()))))
                .andReturn();

    }

    @Test
    public void e_productSelect_expectProductSelectNotFoundAction() throws Exception {

        StartProcess startProcess = this.returnStartProcessObject();
        mockMvc
                .perform(post("/demo/productSelect/" + startProcess.getTaskId())
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("no_match"))
                .andExpect(status().is(404))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

    }

    @Test
    public void f_confirmAction_expectSuccess() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        StartProcess startProcess = this.returnStartProcessObject();

        MvcResult result = mockMvc

                .perform(post("/demo/productSelect/" + startProcess.getTaskId())
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("Telephony"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        ActiveTask task = mapper.readValue(result.getResponse().getContentAsString(), ActiveTask.class);

        mockMvc
                .perform(post("/demo/confirmAction/" + task.getActiveTaskId() + "/true")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.taskId").hasJsonPath())
                .andExpect(jsonPath("$.taskId").value(is(not(isEmptyOrNullString()))))
                .andExpect(jsonPath("$.message").hasJsonPath())
                .andExpect(jsonPath("$.message").value(is(not(isEmptyOrNullString()))))
                .andExpect(jsonPath("$.async").hasJsonPath())
                .andExpect(jsonPath("$.async").value(is(not(isEmptyOrNullString()))))
                .andReturn();

    }

    @Test
    public void g_confirmAction_expectTaskNotFound() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        StartProcess startProcess = this.returnStartProcessObject();

        MvcResult result = mockMvc

                .perform(post("/demo/productSelect/" + startProcess.getTaskId())
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("Internet"))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();

        ActiveTask task = mapper.readValue(result.getResponse().getContentAsString(), ActiveTask.class);

        mockMvc
                .perform(post("/demo/confirmAction/1234567890" + task.getActiveTaskId() + "/true")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(404))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

    }
}