package msd.service;

import msd.config.MsdBackEndApiConfig;
import msd.exception.BackgroundTaskException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class MsdBackEndApiTest {

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private MsdBackEndApiConfig msdBackEndApiConfig;

    private static final String PROCESS_ID = "process_id";

    @InjectMocks
    private MsdBackEndApi msdBackEndApi;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.msdBackEndApiConfig.serviceUrl = "http://localhost";
    }

    @Test
    public void timeoutDelegate_expectSuccess() {
        this.msdBackEndApi.timeoutDelegate(PROCESS_ID);
    }

    @Test(expected = BackgroundTaskException.class)
    public void timeoutDelegate_expectFail() {
        createExceptionMock();
        this.msdBackEndApi.timeoutDelegate(PROCESS_ID);
    }

    @Test
    public void notFoundDelegate() {
        this.msdBackEndApi.notFoundDelegate(PROCESS_ID);
    }

    @Test(expected = BackgroundTaskException.class)
    public void notFoundDelegate_expectFail() {
        createExceptionMock();
        this.msdBackEndApi.notFoundDelegate(PROCESS_ID);
    }

    @Test
    public void savedToDatabase() {
        this.msdBackEndApi.savedToDatabase(PROCESS_ID);
    }

    @Test(expected = BackgroundTaskException.class)
    public void savedToDatabase_expectFail() {
        createExceptionMock();
        this.msdBackEndApi.notFoundDelegate(PROCESS_ID);
    }

    private void createExceptionMock() {
        when(restTemplate.exchange(
                Mockito.anyString(),
                Mockito.eq(HttpMethod.POST),
                Mockito.any(HttpEntity.class),
                Mockito.eq(Void.class))).thenThrow(new RuntimeException());
    }
}