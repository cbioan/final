package api;

import api.domain.ActiveTask;
import api.domain.CompleteTask;
import api.domain.StartProcess;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/camunda")
public interface CamundaDemoApi {

    @GetMapping(value = "/start")
    StartProcess beginProcess();

    @PostMapping(value = "/productSelect/{taskId}")
    ActiveTask productSelect(@RequestBody String productSelect, @PathVariable(value = "taskId") String taskId);

    @PostMapping(value = "/complete/{taskId}/{async}")
    CompleteTask confirmAction(@PathVariable(value = "taskId") String taskId, @PathVariable(value = "async") Boolean async);

}
