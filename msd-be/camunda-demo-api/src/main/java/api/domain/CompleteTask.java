package api.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
public class CompleteTask {

    @NotNull
    private String taskId;
    private String message;
    private Boolean async;

}
