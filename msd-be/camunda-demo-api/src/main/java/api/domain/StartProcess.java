package api.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
public class StartProcess {

    @NotNull
    private String processId;
    @NotNull
    private String taskId;

}
