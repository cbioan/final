package api.exception;

public class ProcessNotFoundException extends RuntimeException {
    private static final String PROCESS_DOES_NOT_EXIST = "The Camunda process could not be found.";

    public ProcessNotFoundException(String m) {
        super(m);
    }

    public ProcessNotFoundException() {
        super(PROCESS_DOES_NOT_EXIST);
    }
}
