package api.exception;

public class TaskNotFoundException extends RuntimeException {
    private static final String TASK_NOT_FOUND = "The Camunda task could not be found.";

    public TaskNotFoundException(String m) {
        super(m);
    }

    public TaskNotFoundException() {
        super(TASK_NOT_FOUND);
    }
}
