package api.exception;

public class ProcessStartFailedException extends RuntimeException {
    private static final String PROCESS_FAILED = "The Camunda process failed to start.";

    public ProcessStartFailedException(String m) {
        super(m);
    }

    public ProcessStartFailedException() {
        super(PROCESS_FAILED);
    }
}
