package impl.application;

import api.LocationApi;
import api.domain.request.LocationRequestDto;
import api.domain.response.LocationDto;
import api.domain.response.LocationsCountDto;
import impl.domain.LocationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
public class LocationController implements LocationApi {

    @Autowired
    private LocationService locationService;

    @PostMapping
    public List<LocationDto> getLocations(@RequestBody LocationRequestDto request) {
        return this.locationService.readLocations(request);
    }

    @PostMapping(value = "/count")
    public LocationsCountDto getLocationsCount(@RequestBody LocationRequestDto request) {
        return this.locationService.readLocationCount(request);
    }
}
