package impl.domain.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationSortItems {

    private String parameter;
    private String direction;
}
