package impl.domain.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationRequestService {
    @JsonProperty("searchcriteria")
    private LocationSearchCriteria locationSearchCriteria;

    @JsonProperty("sortcriteria")
    private LocationSortCriteria locationSortCriteria;
}
