package impl.domain.request;

import api.domain.request.LocationRequestDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@AllArgsConstructor
@Getter
@Setter
public class LocationRequestBodyBuilder {
    private LocationRequestDto requestBody;
    private ArrayList<String> status;
}
