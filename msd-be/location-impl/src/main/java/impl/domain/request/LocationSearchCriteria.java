package impl.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@JsonPropertyOrder({"type", "operator"})
public class LocationSearchCriteria {
    private String operator;
    @JsonProperty("expressions")
    private ArrayList<LocationsSearchExpressions> locationsSearchExpressions;
    private String type;
}
