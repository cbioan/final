package impl.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@JsonRootName(value = "sortcriteria")
public class LocationSortCriteria {

    @JsonProperty("sortitems")
    private ArrayList<LocationSortItems> locationSortItems;
}
