package impl.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
public class SearchExpressionObject {

    private String type;
    private String parameter;
    private String pattern;
    @JsonProperty(value = "values")
    private ArrayList<String> searchForValues;

}
