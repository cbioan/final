package impl.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class LocationsSearchExpressions {
    private String type;
    private String parameter;
    private String pattern;
    private ArrayList<String> values;
    @JsonProperty(value = "ignorecase")
    private Boolean ignoreCase;
}
