package impl.domain;

import api.GoogleService;
import api.domain.GoogleLocationDto;
import api.domain.request.LocationRequestDto;
import api.domain.response.LocationDto;
import api.domain.response.LocationsCountDto;
import impl.domain.response.LocationBranch;
import impl.infrastructure.LocationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class LocationService {

    @Autowired
    private GoogleService googleService;
    @Autowired
    private LocationRepository locationRepository;

    public List<LocationDto> readLocations(LocationRequestDto request){
        return this.extractLocations(this.locationRepository.getLocations(request));
    }

    public LocationsCountDto readLocationCount(LocationRequestDto request){
        return this.locationRepository.getLocationsCount(request);
    }

    private List<LocationDto> extractLocations(List<LocationBranch> branches) {
        List<LocationDto> listOfLocations = new ArrayList<>();
        for (LocationBranch branch : branches) {
            if (branch.getMetadata().getLatitude() == 0 || branch.getMetadata().getLongitude() == 0) {
                this.checkCoordinates(branch);
            }
            listOfLocations.add(branch.getMetadata());
        }
        return listOfLocations;
    }

    private void checkCoordinates(LocationBranch branch) {
        try {
            TimeUnit.MILLISECONDS.sleep(200);
            GoogleLocationDto response = googleService.geoCodeAddress(branch.getMetadata().getAddress());
            branch.getMetadata().setLatitude(response.getLat());
            branch.getMetadata().setLongitude(response.getLng());
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            Thread.currentThread().interrupt();
        }
    }

}
