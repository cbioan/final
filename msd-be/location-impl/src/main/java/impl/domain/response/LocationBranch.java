package impl.domain.response;

import api.domain.response.LocationDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationBranch {
    private LocationDto metadata;
}
