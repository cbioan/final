package impl.domain.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationServiceTree {
    private LocationService services;
}
