package impl.domain.response;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class LocationService {
    private ArrayList<LocationBranch> services;
}
