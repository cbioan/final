package impl.domain.response;

import api.domain.response.LocationsCountDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationServiceCount {
    private LocationsCountDto services;
}
