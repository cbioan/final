package impl.factory;

import api.domain.request.LocationRequestDto;
import api.exception.LocationParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import impl.domain.request.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Component
public class LocationRequestCreateBody {

    private static final String BOOLEAN_EXPR_LIKE = "BooleanExprLike";
    private static final String BOOLEAN_EXPR_IN = "BooleanExprIn";

    private LocationRequestBodyBuilder locationRequestBodyBuilder;

    public String getBody(LocationRequestDto request) {

        try {

            ArrayList<String> status = new ArrayList<>();

            if (request.getStatus().equals(LocationRequestDto.Statuses.LINKED)) {
                status.add(LocationRequestDto.Statuses.LINKED.toString());
            } else {
                status.add(LocationRequestDto.Statuses.UNLINKED.toString());
                status.add(LocationRequestDto.Statuses.INPROGRESS.toString());
            }

            this.locationRequestBodyBuilder = new LocationRequestBodyBuilder(request, status);
            return convertToJson(this.createLocationRequest());

        } catch (JsonProcessingException e) {
            throw new LocationParseException(e.getMessage());
        }
    }

    private LocationRequest createLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        LocationRequestService locationRequestService = this.createLocationRequestService();
        locationRequest.setServices(locationRequestService);
        return locationRequest;
    }

    private LocationRequestService createLocationRequestService() {
        LocationRequestService locationRequestService = new LocationRequestService();
        locationRequestService.setLocationSearchCriteria(this.createLocationSearchCriteriaRequest());
        if (this.locationRequestBodyBuilder.getRequestBody().getDescending() != null
                && this.locationRequestBodyBuilder.getRequestBody().getDescending()) {
            locationRequestService.setLocationSortCriteria(this.appendSortBodyIfDescendingTrue());
        }
        return locationRequestService;
    }

    private LocationSearchCriteria createLocationSearchCriteriaRequest() {
        LocationSearchCriteria locationSearchCriteria = new LocationSearchCriteria();
        locationSearchCriteria.setType("BooleanExprGroup");
        locationSearchCriteria.setOperator("AND");
        locationSearchCriteria.setLocationsSearchExpressions(this.createLocationsSearchExpressionsRequest(locationSearchCriteria));
        return locationSearchCriteria;
    }

    private ArrayList<LocationsSearchExpressions> createLocationsSearchExpressionsRequest(LocationSearchCriteria locationSearchCriteria) {

        ArrayList<LocationsSearchExpressions> locationsSearchArray = new ArrayList<>();

        this.insertBodySearchValues(locationsSearchArray);

        if (locationRequestBodyBuilder.getRequestBody().getSearchPhrase() != null
                && !locationRequestBodyBuilder.getRequestBody().getSearchPhrase().isEmpty()) {
            createLikeExpr(locationSearchCriteria, locationsSearchArray);
        }

        if (locationRequestBodyBuilder.getStatus() != null && !locationRequestBodyBuilder.getStatus().isEmpty()) {
            createInExpr(locationSearchCriteria, locationsSearchArray);
        }

        return locationsSearchArray;
    }

    private void insertBodySearchValues(ArrayList<LocationsSearchExpressions> locationsSearchArray) {
        locationsSearchArray.add(this.createSearchExpressionObject(
                new SearchExpressionObject(
                        BOOLEAN_EXPR_LIKE,
                        "entity",
                        "Branch",
                        null)));

        locationsSearchArray.add(this.createSearchExpressionObject(
                new SearchExpressionObject(
                        BOOLEAN_EXPR_LIKE,
                        "parentserviceid",
                        String.format("*%s*", locationRequestBodyBuilder.getRequestBody().getCustomerId()),
                        null)));
    }

    private void createInExpr(LocationSearchCriteria locationSearchCriteria, ArrayList<LocationsSearchExpressions> locationsSearchArray) {
        //genericString2 means searching by status(linked, unlinked, linking requested)
        locationsSearchArray.add(this.createSearchExpressionObject(
                new SearchExpressionObject(
                        BOOLEAN_EXPR_IN,
                        "genericString2",
                        null,
                        locationRequestBodyBuilder.getStatus())));
        locationSearchCriteria.setLocationsSearchExpressions(locationsSearchArray);
    }

    private void createLikeExpr(LocationSearchCriteria locationSearchCriteria, ArrayList<LocationsSearchExpressions> locationsSearchArray) {
        //genericString1 means searching by branch name
        locationsSearchArray.add(this.createSearchExpressionObject(
                new SearchExpressionObject(
                        BOOLEAN_EXPR_LIKE,
                        "genericString1",
                        String.format("*%s*", locationRequestBodyBuilder.getRequestBody().getSearchPhrase()),
                        null)));
        locationSearchCriteria.setLocationsSearchExpressions(locationsSearchArray);
    }

    private LocationsSearchExpressions createSearchExpressionObject(SearchExpressionObject searchExpressionObject) {
        LocationsSearchExpressions locationsSearchExpression = new LocationsSearchExpressions();
        locationsSearchExpression.setType(searchExpressionObject.getType());
        locationsSearchExpression.setIgnoreCase(true);
        locationsSearchExpression.setParameter(searchExpressionObject.getParameter());
        locationsSearchExpression.setPattern(searchExpressionObject.getPattern());
        locationsSearchExpression.setValues(searchExpressionObject.getSearchForValues());
        return locationsSearchExpression;
    }

    private LocationSortCriteria appendSortBodyIfDescendingTrue() {
        LocationSortCriteria locationSortCriteria = new LocationSortCriteria();
        LocationSortItems locationSortItem = new LocationSortItems();
        ArrayList<LocationSortItems> locationSortItems = new ArrayList<>();
        locationSortItem.setParameter("servicename");
        locationSortItem.setDirection("descending");
        locationSortItems.add(locationSortItem);
        locationSortCriteria.setLocationSortItems(locationSortItems);
        return locationSortCriteria;
    }

    private String convertToJson(LocationRequest requestObject) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(requestObject);
    }


}
