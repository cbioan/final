package impl.factory;

import api.domain.request.LocationRequestDto;
import impl.config.ServiceDirectorConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Slf4j
@Component
public class LocationRequestCreateUrl {

    @Autowired
    private ServiceDirectorConfig config;

    private LocationRequestCreateUrl() {
    }

    private static final String[] URL_PARTS = {
            "/v2/services?limit=",
            "&fromrow=",
            "&withmetadata=true",
            "&specpkgname=",
            "&tenantid="
    };

    public String getUrl(LocationRequestDto requestBody) {

        Integer page = requestBody.getPage();
        Integer resultsPerPage = requestBody.getResultsPerPage();

        if (page > 0) {
            return createUrlWithPagination(page - 1, resultsPerPage);
        } else {
            return urlWithoutPagination();
        }

    }

    private String createUrlWithPagination(Integer page, Integer resultsPerPage) {
        if (resultsPerPage == 0) {
            resultsPerPage = Integer.parseInt(this.config.batchLimit);
        }
        return this.urlWithPagination(resultsPerPage, page * resultsPerPage);
    }

    private String urlWithPagination(Integer resultsPerPage, Integer row) {

        return
                this.config.serviceUrl +
                        LocationRequestCreateUrl.URL_PARTS[0] +
                        resultsPerPage +
                        LocationRequestCreateUrl.URL_PARTS[1] +
                        row +
                        LocationRequestCreateUrl.URL_PARTS[2] +
                        LocationRequestCreateUrl.URL_PARTS[3] +
                        this.config.customerBranchDescriptor +
                        LocationRequestCreateUrl.URL_PARTS[4] +
                        this.config.requestBranchTenantId;

    }

    private String urlWithoutPagination() {

        return
                this.config.serviceUrl +
                        LocationRequestCreateUrl.URL_PARTS[0] +
                        this.config.batchLimit +
                        LocationRequestCreateUrl.URL_PARTS[1] +
                        0 +
                        LocationRequestCreateUrl.URL_PARTS[2] +
                        LocationRequestCreateUrl.URL_PARTS[3] +
                        this.config.customerBranchDescriptor +
                        LocationRequestCreateUrl.URL_PARTS[4] +
                        this.config.requestBranchTenantId;
    }

}
