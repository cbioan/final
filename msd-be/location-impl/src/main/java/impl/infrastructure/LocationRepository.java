package impl.infrastructure;

import api.ServiceDirector;
import api.domain.request.LocationRequestDto;
import api.domain.response.LocationsCountDto;
import api.exception.LocationNotFoundException;
import api.exception.LocationParseException;
import api.exception.ServiceAuthenticationFailedException;
import exception.SdAuthenticationFailedException;
import exception.SdProcessingException;
import exception.SdResultsNotFoundException;
import impl.domain.response.LocationServiceCount;
import impl.factory.LocationRequestCreateBody;
import impl.factory.LocationRequestCreateUrl;
import impl.domain.response.LocationBranch;
import impl.domain.response.LocationServiceTree;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.function.Supplier;

@Slf4j
@Repository
public class LocationRepository {

    @Autowired
    private ServiceDirector serviceDirector;
    @Autowired
    private LocationRequestCreateUrl locationRequestCreateUrl;
    @Autowired
    private LocationRequestCreateBody locationRequestCreateBody;

    public List<LocationBranch> getLocations(LocationRequestDto request) {
        return this.repositoryProxy(() -> this.performLocationsRequest(request));
    }

    public LocationsCountDto getLocationsCount(LocationRequestDto request) {
        return this.repositoryProxy(() -> this.performLocationsCount(request));
    }

    private <R> R repositoryProxy(Supplier<R> func) {
        try {
            return func.get();
        } catch (SdAuthenticationFailedException e) {
            throw new ServiceAuthenticationFailedException(e.getMessage());
        } catch (SdResultsNotFoundException e) {
            throw new LocationNotFoundException(e.getMessage());
        } catch (SdProcessingException e) {
            throw new LocationParseException(e.getMessage());
        }
    }

    private LocationsCountDto performLocationsCount(LocationRequestDto request) {
        String body = locationRequestCreateBody.getBody(request);
        String url = locationRequestCreateUrl.getUrl(request);
        LocationServiceCount serviceCount = this.serviceDirector.getLocations(url, body, LocationServiceCount.class);
        return serviceCount.getServices();
    }

    private List<LocationBranch> performLocationsRequest(LocationRequestDto request) {
        String body = locationRequestCreateBody.getBody(request);
        String url = locationRequestCreateUrl.getUrl(request);
        LocationServiceTree response = this.serviceDirector.getLocations(url, body, LocationServiceTree.class);
        return response.getServices().getServices();
    }
}
