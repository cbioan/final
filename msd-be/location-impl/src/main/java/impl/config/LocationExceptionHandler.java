package impl.config;

import api.exception.LocationNotFoundException;
import api.exception.LocationParseException;
import api.exception.ServiceAuthenticationFailedException;
import common.error.ApiError;
import common.error.RestExceptionHandler;
import impl.application.LocationController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@RestControllerAdvice(assignableTypes = LocationController.class)
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class LocationExceptionHandler extends RestExceptionHandler {

    private ResponseEntity<Object> setLocationException(HttpStatus unauthorized, String message) {
        ApiError apiError = new ApiError(unauthorized);
        apiError.setMessage(message);
        log.error(message);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(ServiceAuthenticationFailedException.class)
    protected ResponseEntity<Object> handleException(ServiceAuthenticationFailedException ex) {
        return setLocationException(UNAUTHORIZED, ex.getMessage());
    }

    @ExceptionHandler(LocationNotFoundException.class)
    protected ResponseEntity<Object> handleException(LocationNotFoundException ex) {
        return setLocationException(NOT_FOUND, ex.getMessage());
    }

    @ExceptionHandler(LocationParseException.class)
    protected ResponseEntity<Object> handleException(LocationParseException ex) {
        return setLocationException(INTERNAL_SERVER_ERROR, ex.getMessage());
    }

}
