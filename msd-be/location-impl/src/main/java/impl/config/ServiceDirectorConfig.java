package impl.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:service-director-${spring.profiles.active}.properties")
@Configuration
public class ServiceDirectorConfig {

    @Value("${hpe.serviceUrl}")
    public String serviceUrl;
    @Value("${hpe.serviceAuthTenantName}")
    public String serviceAuthTenantName;
    @Value("${hpe.requestBranchTenantId}")
    public String requestBranchTenantId;
    @Value("${hpe.batchLimit}")
    public String batchLimit;
    @Value("${hpe.customerBranchDescriptor}")
    public String customerBranchDescriptor;
    @Value("${hpe.sortingSelection}")
    public String sortingSelection;

}
