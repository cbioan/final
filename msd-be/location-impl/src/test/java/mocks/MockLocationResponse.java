package mocks;

import api.domain.response.LocationDto;
import impl.domain.response.LocationBranch;
import impl.domain.response.LocationService;
import impl.domain.response.LocationServiceTree;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MockLocationResponse {

    public static List<LocationBranch> mockLocationResponseBranch(int results) {

        return locationBranchList(results);

    }

    public static List<LocationBranch> mockLocationResponseBranchForSearch(String searchText, int results) {

        ArrayList<LocationBranch> locationBranch = createBranchListForStringSearch(results);
        ArrayList<LocationBranch> locationBranches = new ArrayList<>();

        for (LocationBranch aLocationBranch : locationBranch) {
            if (aLocationBranch.getMetadata().getServiceName().equals(searchText)) {
                locationBranches.add(aLocationBranch);
            }
        }
        return locationBranches;
    }


    public static LocationServiceTree mockLocationResponseTree(int results) {

        if (results <= 0) {
            return new LocationServiceTree();
        }

        LocationServiceTree mockResponseBody = new LocationServiceTree();
        LocationService locationService = new LocationService();

        ArrayList<LocationBranch> listOfLocations = locationBranchList(results);

        locationService.setServices(listOfLocations);
        mockResponseBody.setServices(locationService);

        return mockResponseBody;

    }

    public static LocationServiceTree mockLocationSearchResponseTree(int results) {

        if (results <= 0) {
            return new LocationServiceTree();
        }

        LocationServiceTree mockResponseBody = new LocationServiceTree();
        LocationService locationService = new LocationService();

        ArrayList<LocationBranch> listOfLocations = createBranchListForStringSearch(results);

        locationService.setServices(listOfLocations);
        mockResponseBody.setServices(locationService);

        return mockResponseBody;

    }

    private static ArrayList<LocationBranch> locationBranchList(int results) {

        ArrayList<LocationBranch> listOfLocations = new ArrayList<>();

        generateLocationBranches(results, listOfLocations);

        return listOfLocations;
    }

    private static ArrayList<LocationBranch> createBranchListForStringSearch(int results) {

        Random r = new Random();

        int Low = 1;
        int High = 10;
        int rand;

        ArrayList<LocationBranch> listOfLocations = new ArrayList<>();

        LocationDto location = new LocationDto();
        LocationBranch locationBranch = new LocationBranch();
        location.setPostalCode(generateRandomInteger(6));
        location.setAddress(generateRandomWords(5));
        location.setLocationName(generateRandomWords(3));
        location.setCountry(generateRandomWords(1));
        location.setHouseNumber(generateRandomInteger(2));
        location.setState(generateRandomWords(1));
        location.setStreet(generateRandomWords(3));
        location.setUuid(generateRandomInteger(12));
        location.setTenantId(generateRandomWords(1));
        location.setServiceName(generateRandomWords(1));
        location.setStatus(generateRandomWords(1));
        locationBranch.setMetadata(location);
        listOfLocations.add(locationBranch);

        generateLocationBranches(results, listOfLocations);
        return listOfLocations;
    }

    private static void generateLocationBranches(int results, ArrayList<LocationBranch> listOfLocations) {

        Random r = new Random();

        int low = 1;
        int high = 10;
        int rand;

        for (int i = 0; i < results; i++) {

            LocationDto locationIterator = new LocationDto();

            LocationBranch locationBranchIterator = new LocationBranch();

            locationIterator.setPostalCode(generateRandomInteger(6));

            rand = r.nextInt(high - low) + low;
            locationIterator.setAddress(generateRandomWords(rand));

            rand = r.nextInt(high - low) + low;
            locationIterator.setLocationName(generateRandomWords(rand));

            rand = r.nextInt(high - low) + low;
            locationIterator.setCountry(generateRandomWords(rand));

            locationIterator.setHouseNumber(generateRandomInteger(2));

            rand = r.nextInt(high - low) + low;
            locationIterator.setState(generateRandomWords(rand));

            rand = r.nextInt(high - low) + low;
            locationIterator.setStreet(generateRandomWords(rand));

            locationIterator.setUuid(generateRandomInteger(12));

            rand = r.nextInt(high - low) + low;
            locationIterator.setLocationName(generateRandomWords(rand));

            rand = r.nextInt(high - low) + low;
            locationIterator.setTenantId(generateRandomWords(rand));

            rand = r.nextInt(high - low) + low;
            locationIterator.setServiceName(generateRandomWords(rand));

            locationBranchIterator.setMetadata(locationIterator);
            listOfLocations.add(locationBranchIterator);

        }
    }

    private static String generateRandomInteger(int length) {

        Random r = new Random();
        int Low = 1;
        int High = 10;
        int rand;

        String concatenatedString = "";

        for (int i = 0; i < length; i++) {
            rand = r.nextInt(High - Low) + Low;
            concatenatedString = concatenatedString.concat(Integer.toString(rand));
        }

        return concatenatedString;

    }

    private static String generateRandomWords(int numberOfWords) {
        String[] randomStrings = new String[numberOfWords];
        Random random = new Random();

        for (int i = 0; i < numberOfWords; i++) {
            char[] word = new char[random.nextInt(8) + 3];
            for (int j = 0; j < word.length; j++) {
                word[j] = (char) ('a' + random.nextInt(26));
            }
            randomStrings[i] = new String(word);
        }

        return String.join(" ", randomStrings);
    }

}
