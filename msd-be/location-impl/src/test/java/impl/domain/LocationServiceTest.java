package impl.domain;

import api.GoogleService;
import api.domain.GoogleLocationDto;
import api.domain.request.LocationRequestDto;
import api.domain.response.LocationDto;
import api.domain.response.LocationsCountDto;
import impl.domain.response.LocationBranch;
import impl.infrastructure.LocationRepository;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static mocks.MockLocationResponse.mockLocationResponseBranch;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class LocationServiceTest {

    @Mock
    GoogleService googleService;

    @Mock
    LocationRepository locationRepository;

    @InjectMocks
    private LocationService locationService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void readLocations_expectSuccess() {
        LocationRequestDto mockRequest = createRequestDtoMock();
        List<LocationBranch> mockHpeServiceDirectorLocations = mockLocationResponseBranch(5);
        when(googleService.geoCodeAddress(Mockito.anyString()))
                .thenReturn(new GoogleLocationDto(123.00, 47.00));
        when(locationRepository.getLocations(Mockito.any(LocationRequestDto.class)))
                .thenReturn(mockHpeServiceDirectorLocations);
        List<LocationDto> response = locationService.readLocations(mockRequest);
        assertNotEquals(response, IsEmptyCollection.empty());
        assertThat(response, hasSize(5));
    }

    @Test
    public void readLocationsCount_expectSuccess() {
        LocationRequestDto mockRequest = createRequestDtoMock();
        LocationsCountDto mockCount = new LocationsCountDto();
        mockCount.setServiceCount(56);
        when(locationRepository.getLocationsCount(Mockito.any(LocationRequestDto.class)))
                .thenReturn(mockCount);
        LocationsCountDto response = locationService.readLocationCount(mockRequest);
        assertNotNull(response.getServiceCount());
        assertEquals(56, response.getServiceCount().intValue());
    }

    private LocationRequestDto createRequestDtoMock() {
        LocationRequestDto mockRequest = new LocationRequestDto();
        mockRequest.setCustomerId("apple");
        mockRequest.setStatus(LocationRequestDto.Statuses.LINKED);
        mockRequest.setDescending(false);
        return mockRequest;
    }
}