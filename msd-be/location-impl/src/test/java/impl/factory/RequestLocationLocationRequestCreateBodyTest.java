package impl.factory;

import api.domain.request.LocationRequestDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.Matchers.isEmptyString;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class RequestLocationLocationRequestCreateBodyTest {

    @InjectMocks
    LocationRequestCreateBody locationRequestCreateBody;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private LocationRequestDto mockLinked() {
        LocationRequestDto mockRequest = new LocationRequestDto();
        mockRequest.setSearchPhrase("hello world");
        mockRequest.setStatus(LocationRequestDto.Statuses.LINKED);
        mockRequest.setCustomerId("customer");
        mockRequest.setDescending(true);
        return mockRequest;
    }

    private LocationRequestDto mockUnlinked() {
        LocationRequestDto mockRequest = new LocationRequestDto();
        mockRequest.setSearchPhrase("hello world");
        mockRequest.setStatus(LocationRequestDto.Statuses.UNLINKED);
        mockRequest.setCustomerId("customer");
        mockRequest.setDescending(true);
        return mockRequest;
    }

    @Test
    public void getBody_expectSuccess_linked() {
        LocationRequestDto mockRequest = mockLinked();
        String response = locationRequestCreateBody.getBody(mockRequest);
        Assert.assertNotNull(response);
        Assert.assertNotEquals(response, isEmptyString());
    }

    @Test
    public void getBody_expectSuccess_notlinked() {
        LocationRequestDto mockRequest = mockUnlinked();
        String response = locationRequestCreateBody.getBody(mockRequest);
        Assert.assertNotNull(response);
        Assert.assertNotEquals(response, isEmptyString());
    }
}