package impl.factory;

import api.domain.request.LocationRequestDto;
import impl.config.ServiceDirectorConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.Matchers.isEmptyString;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class LocationRequestCreateUrlTest {

    @Mock
    private ServiceDirectorConfig config;
    @InjectMocks
    LocationRequestCreateUrl locationRequestCreateUrl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.config.batchLimit = "20";
        this.config.customerBranchDescriptor = "string";
        this.config.requestBranchTenantId = "tenantId";
        this.config.serviceUrl = "http://test.com";
        this.config.sortingSelection = "servicename";
        this.config.serviceAuthTenantName = "tenant";
    }

    @Test
    public void getUrl_expect_Success_withPagination() {

        LocationRequestDto mockRequest = new LocationRequestDto();
        mockRequest.setPage(1);

        String response = locationRequestCreateUrl.getUrl(mockRequest);
        Assert.assertNotNull(response);
        Assert.assertNotEquals(response, isEmptyString());
    }

    @Test
    public void getUrl_expect_Success_withoutPagination() {

        LocationRequestDto mockRequest = new LocationRequestDto();
        mockRequest.setPage(0);

        String response = locationRequestCreateUrl.getUrl(mockRequest);
        Assert.assertNotNull(response);
        Assert.assertNotEquals(response, isEmptyString());
    }
}