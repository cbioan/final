package impl.infrastructure;

import api.ServiceDirector;
import api.domain.request.LocationRequestDto;
import api.domain.response.LocationsCountDto;
import api.exception.LocationNotFoundException;
import api.exception.LocationParseException;
import api.exception.ServiceAuthenticationFailedException;
import exception.SdAuthenticationFailedException;
import exception.SdProcessingException;
import exception.SdResultsNotFoundException;
import impl.domain.response.LocationBranch;
import impl.domain.response.LocationService;
import impl.domain.response.LocationServiceCount;
import impl.domain.response.LocationServiceTree;
import impl.factory.LocationRequestCreateBody;
import impl.factory.LocationRequestCreateUrl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class LocationRepositoryTest {

    @Mock
    LocationRequestCreateUrl locationRequestCreateUrl;
    @Mock
    LocationRequestCreateBody locationRequestCreateBody;
    @Mock
    ServiceDirector serviceDirector;

    private LocationServiceTree mockLocationServiceTree;
    private LocationServiceCount mockLocationServiceCount;

    @InjectMocks
    private LocationRepository locationRepository;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    private void mockTreeSuccess() {
        LocationServiceTree mockResult = new LocationServiceTree();
        LocationService mockLocationService = new LocationService();
        ArrayList<LocationBranch> mockBranches = new ArrayList<>();
        mockResult.setServices(mockLocationService);
        mockLocationService.setServices(mockBranches);
        this.mockLocationServiceTree = mockResult;
    }

    private void mockServiceCount() {
        LocationServiceCount mockResult = new LocationServiceCount();
        LocationsCountDto locationsCount = new LocationsCountDto();
        locationsCount.setServiceCount(new Random().nextInt(50) + 1);
        mockResult.setServices(locationsCount);
        this.mockLocationServiceCount = mockResult;
    }

    private void mockTreeSuccess_with_body_and_url() {
        this.mockTreeSuccess();
        this.createRequestResponseMocks();
    }

    private void mockServiceCount_with_body_and_url() {
        this.mockServiceCount();
        this.createRequestResponseMocks();
    }

    private void createRequestResponseMocks() {
        when(locationRequestCreateBody.getBody(
                Mockito.any(LocationRequestDto.class))).thenReturn("body");
        when(locationRequestCreateUrl.getUrl(
                Mockito.any(LocationRequestDto.class))).thenReturn("url");
    }

    @Test
    public void getLocations_expectSuccess() {

        mockTreeSuccess_with_body_and_url();

        when(serviceDirector.getLocations(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.eq(LocationServiceTree.class))).thenReturn(this.mockLocationServiceTree);

        List<LocationBranch> response = locationRepository.getLocations(new LocationRequestDto());

        Assert.assertNotNull(response);

    }

    @Test(expected = LocationNotFoundException.class)
    public void getLocations_expect_BodyBuilderFail() {

        this.mockTreeSuccess();

        when(locationRequestCreateBody.getBody(
                Mockito.any(LocationRequestDto.class))).thenThrow(new SdResultsNotFoundException());

        locationRepository.getLocations(new LocationRequestDto());

    }

    @Test(expected = ServiceAuthenticationFailedException.class)
    public void getLocations_expect_ServiceDirectorFail_Auth() {

        mockTreeSuccess_with_body_and_url();

        when(serviceDirector.getLocations(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.eq(LocationServiceTree.class))).thenThrow(new SdAuthenticationFailedException());

        locationRepository.getLocations(new LocationRequestDto());

    }

    @Test(expected = LocationNotFoundException.class)
    public void getLocations_expect_ServiceDirectorFail_EmptyResults() {

        mockTreeSuccess_with_body_and_url();

        when(serviceDirector.getLocations(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.eq(LocationServiceTree.class))).thenThrow(new SdResultsNotFoundException());

        locationRepository.getLocations(new LocationRequestDto());

    }

    @Test(expected = LocationParseException.class)
    public void getLocations_expect_ServiceDirectorFail_UnexpectedFailure() {

        mockTreeSuccess_with_body_and_url();

        when(serviceDirector.getLocations(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.eq(LocationServiceTree.class))).thenThrow(new SdProcessingException());

        locationRepository.getLocations(new LocationRequestDto());

    }

    @Test
    public void getLocationsCount_expectSuccess() {

        mockServiceCount_with_body_and_url();

        when(serviceDirector.getLocations(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.eq(LocationServiceCount.class))).thenReturn(this.mockLocationServiceCount);

        LocationsCountDto response = locationRepository.getLocationsCount(new LocationRequestDto());
        Assert.assertNotNull(response);

    }

    @Test(expected = ServiceAuthenticationFailedException.class)
    public void getLocationsCount_expect_ServiceDirectorFail_Auth() {

        mockServiceCount_with_body_and_url();

        when(serviceDirector.getLocations(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.eq(LocationServiceCount.class))).thenThrow(new SdAuthenticationFailedException());

        locationRepository.getLocationsCount(new LocationRequestDto());

    }

    @Test(expected = LocationNotFoundException.class)
    public void getLocationsCount_expect_ServiceDirectorFail_EmptyResults() {

        mockServiceCount_with_body_and_url();

        when(serviceDirector.getLocations(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.eq(LocationServiceCount.class))).thenThrow(new SdResultsNotFoundException());

        locationRepository.getLocationsCount(new LocationRequestDto());

    }

    @Test(expected = LocationParseException.class)
    public void getLocationsCount_expect_ServiceDirectorFail_UnexpectedFailure() {

        mockServiceCount_with_body_and_url();

        when(serviceDirector.getLocations(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.eq(LocationServiceCount.class))).thenThrow(new SdProcessingException());

        locationRepository.getLocationsCount(new LocationRequestDto());

    }
}