package impl.application;

import api.GoogleService;
import api.domain.GoogleLocationDto;
import api.domain.request.LocationRequestDto;
import api.domain.response.LocationsCountDto;
import api.exception.LocationNotFoundException;
import api.exception.LocationParseException;
import api.exception.ServiceAuthenticationFailedException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import impl.config.LocationExceptionHandler;
import impl.domain.LocationService;
import impl.domain.response.LocationBranch;
import impl.infrastructure.LocationRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static mocks.MockLocationResponse.mockLocationResponseBranch;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {LocationController.class, LocationExceptionHandler.class})
@AutoConfigureMockMvc
@AutoConfigureWebMvc
@EnableAutoConfiguration
public class LocationControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    LocationRepository locationRepository;
    @MockBean
    GoogleService googleService;
    @MockBean
    RestTemplate restTemplate;
    @SpyBean
    private LocationService locationService;

    @InjectMocks
    LocationController locationController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getLocations_expectSuccess_FullRequest() throws Exception {
        performPostRequest(0.0, 0.0, mockRequest_FullRequest());
    }

    @Test
    public void getLocations_expectSuccess_MinRequest() throws Exception {
        performPostRequest(123.432, 45.564, mockRequest_MinimumRequest());
    }

    @Test
    public void getLocations_expectFailure() throws Exception {
        performPostRequest_Exception(mockRequest_FullRequest(), ServiceAuthenticationFailedException.class, 401);
    }

    @Test
    public void getLocations_expectFailure_Empty() throws Exception {
        performPostRequest_Exception(mockRequest_FullRequest(), LocationNotFoundException.class, 404);
    }

    @Test
    public void getLocations_expectFailure_Unexpected() throws Exception {
        performPostRequest_Exception(mockRequest_FullRequest(), LocationParseException.class, 500);
    }

    @Test
    public void getLocationsCount_expectSuccess_MinRequest() throws Exception {
        performPostRequestCount(mockRequest_MinimumRequest());
    }

    @Test
    public void getLocationsCount_expectFailure() throws Exception {
        performPostRequestCount_Exception(mockRequest_MinimumRequest(), ServiceAuthenticationFailedException.class, 401);
    }

    @Test
    public void getLocationsCount_expectFailure_Empty() throws Exception {
        performPostRequestCount_Exception(mockRequest_MinimumRequest(), LocationNotFoundException.class, 404);
    }

    @Test
    public void getLocationsCount_expectFailure_Unexpected() throws Exception {
        performPostRequestCount_Exception(mockRequest_MinimumRequest(), LocationParseException.class, 500);
    }

    private String mockRequest_FullRequest() throws JsonProcessingException {
        LocationRequestDto mockRequest = new LocationRequestDto();
        mockRequest.setStatus(LocationRequestDto.Statuses.LINKED);
        mockRequest.setCustomerId("customer");
        mockRequest.setSearchPhrase("hello world");
        mockRequest.setPage(1);
        mockRequest.setResultsPerPage(10);
        mockRequest.setDescending(true);
        return this.convertToJson(mockRequest);
    }

    private String mockRequest_MinimumRequest() throws JsonProcessingException {
        LocationRequestDto mockRequest = new LocationRequestDto();
        mockRequest.setStatus(LocationRequestDto.Statuses.LINKED);
        mockRequest.setCustomerId("customer");
        return this.convertToJson(mockRequest);
    }

    private String convertToJson(LocationRequestDto requestObject) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(requestObject);
    }

    private void performPostRequest(double v, double v2, String s) throws Exception {
        List<LocationBranch> mockHpeServiceDirectorLocations = mockLocationResponseBranch(5);

        when(locationRepository.getLocations(Mockito.any(LocationRequestDto.class)))
                .thenReturn(mockHpeServiceDirectorLocations);

        when(googleService.geoCodeAddress(Mockito.anyString())).
                thenReturn(new GoogleLocationDto(v, v2));

        mockMvc
                .perform(post("/location")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(s))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(greaterThanOrEqualTo(0))))
                .andExpect(jsonPath("$.[*]").hasJsonPath())
                .andExpect(jsonPath("$.[*].address").hasJsonPath())
                .andExpect(jsonPath("$.[*].country").hasJsonPath())
                .andExpect(jsonPath("$.[*].street").hasJsonPath())
                .andExpect(jsonPath("$.[*].state").hasJsonPath())
                .andExpect(jsonPath("$.[*].postal_code").hasJsonPath())
                .andExpect(jsonPath("$.[*].house_number").hasJsonPath())
                .andExpect(jsonPath("$.[*].location_name").hasJsonPath())
                .andExpect(jsonPath("$.[*].tenantid").hasJsonPath())
                .andExpect(jsonPath("$.[*].servicename").hasJsonPath())
                .andExpect(jsonPath("$.[*].uuid").hasJsonPath())
                .andExpect(jsonPath("$.[*].latitude").hasJsonPath())
                .andExpect(jsonPath("$.[*].longitude").hasJsonPath())
                .andReturn();
    }

    private void performPostRequest_Exception(String s, Class<? extends RuntimeException> clazz, Integer statusCode) throws Exception {

        when(locationRepository.getLocations(Mockito.any(LocationRequestDto.class)))
                .thenThrow(clazz);

        when(googleService.geoCodeAddress(Mockito.anyString())).
                thenReturn(new GoogleLocationDto(0.0, 0.0));

        mockMvcRequest(s, statusCode, "/location");
    }

    private void performPostRequestCount(String s) throws Exception {

        LocationsCountDto mockHpeServiceCount = new LocationsCountDto();
        mockHpeServiceCount.setServiceCount(5);

        when(locationRepository.getLocationsCount(Mockito.any(LocationRequestDto.class)))
                .thenReturn(mockHpeServiceCount);

        mockMvc
                .perform(post("/location/count")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(s))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.servicecount").hasJsonPath())
                .andReturn();
    }

    private void performPostRequestCount_Exception(String s, Class<? extends RuntimeException> clazz, Integer statusCode) throws Exception {

        when(locationRepository.getLocationsCount(Mockito.any(LocationRequestDto.class)))
                .thenThrow(clazz);

        mockMvcRequest(s, statusCode, "/location/count");
    }

    private void mockMvcRequest(String s, Integer statusCode, String s2) throws Exception {
        mockMvc
                .perform(post(s2)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(s))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is(statusCode))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
    }
}