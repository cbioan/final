package api;

public interface ServiceDirector {

    <R> R getLocations(String url, String body, Class<R> returnEntity);

}
