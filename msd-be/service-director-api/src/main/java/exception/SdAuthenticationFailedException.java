package exception;

public class SdAuthenticationFailedException extends RuntimeException {
    private static final String FAILED_SERVICE_AUTHENTICATION = "Could not authenticate with service director.";

    public SdAuthenticationFailedException(String m) {
        super(m);
    }

    public SdAuthenticationFailedException() {
        super(FAILED_SERVICE_AUTHENTICATION);
    }
}
