package exception;

public class SdProcessingException extends RuntimeException {
    private static final String UNEXPECTED_ERROR = "Un unexpected error occured.";

    public SdProcessingException(String m) {
        super(m);
    }

    public SdProcessingException() {
        super(UNEXPECTED_ERROR);
    }
}
