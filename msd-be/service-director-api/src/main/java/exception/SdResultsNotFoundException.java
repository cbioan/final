package exception;

public class SdResultsNotFoundException extends RuntimeException {
    private static final String RESULTS_NOT_FOUND = "Results not found.";

    public SdResultsNotFoundException(String m) {
        super(m);
    }

    public SdResultsNotFoundException() {
        super(RESULTS_NOT_FOUND);
    }
}
