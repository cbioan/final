package impl.infrastructure;

import api.domain.ActiveTask;
import api.domain.CompleteTask;
import api.domain.StartProcess;
import api.exception.ProcessStartFailedException;
import api.exception.TaskNotFoundException;
import impl.config.CamundaConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class CamundaDemoService {

    @Autowired
    private RestTemplate restTemplate;
    private HttpHeaders headers;
    private String serviceUrl;

    public CamundaDemoService(CamundaConfig config) {
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        this.serviceUrl = config.serviceUrl + "/demo";
    }

    public StartProcess camundaStartProcess() {

        try {
            ResponseEntity<StartProcess> response = restTemplate.exchange(this.serviceUrl, HttpMethod.GET, new HttpEntity<>(headers), StartProcess.class);
            if (response != null) {
                return response.getBody();
            } else {
                throw new ProcessStartFailedException();
            }
        } catch (HttpClientErrorException e) {
            throw new ProcessStartFailedException();
        }

    }

    public ActiveTask camundaProductSelect(String taskId, String body) {

        String callUrl = this.serviceUrl + "/productSelect/" + taskId;

        try {
            ResponseEntity<ActiveTask> response = restTemplate.exchange(callUrl, HttpMethod.POST, new HttpEntity<>(body, headers), ActiveTask.class);
            if (response != null) {
                return response.getBody();
            } else {
                throw new TaskNotFoundException();
            }
        } catch (HttpClientErrorException e) {
            throw new TaskNotFoundException();
        }

    }

    public CompleteTask camundaCompleteTask(String taskId, Boolean async) {

        String callUrl = this.serviceUrl + "/confirmAction/" + taskId + "/" + async;

        try {
            ResponseEntity<CompleteTask> response = restTemplate.exchange(callUrl, HttpMethod.POST, new HttpEntity<>(headers), CompleteTask.class);
            if (response != null) {
                return response.getBody();
            } else {
                throw new TaskNotFoundException();
            }
        } catch (HttpClientErrorException e) {
            throw new TaskNotFoundException();
        }

    }

}
