package impl.application;

import api.CamundaDemoApi;
import api.domain.ActiveTask;
import api.domain.CompleteTask;
import api.domain.StartProcess;
import impl.infrastructure.CamundaDemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.*;
import websocket.domain.MsdMessage;

@Slf4j
@RestController
public class CamundaDemoApiImpl implements CamundaDemoApi {

    @Autowired
    private CamundaDemoService camunda;
    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    private static final String WEB_SOCKET_TOPIC_ENDPOINT = "/topic/";

    @GetMapping(value = "/start")
    public StartProcess beginProcess() {
        return camunda.camundaStartProcess();
    }

    @PostMapping(value = "/productSelect/{taskId}")
    public ActiveTask productSelect(@RequestBody String productSelect, @PathVariable(value = "taskId") String taskId) {
        return camunda.camundaProductSelect(taskId, productSelect);
    }

    @PostMapping(value = "/complete/{taskId}/{async}")
    public CompleteTask confirmAction(@PathVariable(value = "taskId") String taskId,
                                      @PathVariable(value = "async") Boolean async) {
        return camunda.camundaCompleteTask(taskId, async);
    }

    @PostMapping(value = "/timeout/{processId}")
    public void timeoutDelegate(@PathVariable(value = "processId") String processId) {
        log.info("Timeout delegate called for process {}", processId);
        sendWebSocketMessage(processId, "Your session has timed out", MsdMessage.MessageType.TIMEOUT);
    }

    @PostMapping(value = "/notfound/{processId}")
    public void notFoundDelegate(@PathVariable(value = "processId") String processId) {
        log.info("Not found delegate called for process {}", processId);
        sendWebSocketMessage(processId, "Your selection returned NOT FOUND service", MsdMessage.MessageType.NOT_FOUND);
    }

    @PostMapping(value = "/service/{processId}")
    public void saveDelegate(@PathVariable(value = "processId") String processId) {
        log.info("Save delegate called for process {}", processId);
        sendWebSocketMessage(processId, "Your selection has been saved", MsdMessage.MessageType.COMPLETE);
    }

    private void sendWebSocketMessage(String processId, String s, MsdMessage.MessageType messageType) {
        MsdMessage msdMessage = new MsdMessage();
        msdMessage.setContent(s);
        msdMessage.setType(messageType);
        messagingTemplate.convertAndSend(WEB_SOCKET_TOPIC_ENDPOINT + processId, msdMessage);
    }

}
