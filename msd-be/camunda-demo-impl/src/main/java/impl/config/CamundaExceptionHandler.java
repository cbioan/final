package impl.config;

import api.exception.ProcessNotFoundException;
import api.exception.ProcessStartFailedException;
import api.exception.TaskNotFoundException;
import common.error.ApiError;
import common.error.RestExceptionHandler;
import impl.application.CamundaDemoApiImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice(assignableTypes = CamundaDemoApiImpl.class)
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class CamundaExceptionHandler extends RestExceptionHandler {

    private ResponseEntity<Object> setNotFound(String message) {
        ApiError apiError = new ApiError(NOT_FOUND);
        apiError.setMessage(message);
        log.error(message);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(ProcessNotFoundException.class)
    protected ResponseEntity<Object> handleException(ProcessNotFoundException ex) {
        return setNotFound(ex.getMessage());
    }

    @ExceptionHandler(TaskNotFoundException.class)
    protected ResponseEntity<Object> handleException(TaskNotFoundException ex) {
        return setNotFound(ex.getMessage());
    }

    @ExceptionHandler(ProcessStartFailedException.class)
    protected ResponseEntity<Object> handleException(ProcessStartFailedException ex) {
        return setNotFound(ex.getMessage());
    }

}
