package impl.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.*;
import org.springframework.web.client.RestTemplate;

@Configuration
@PropertySource(value = "classpath:camunda-${spring.profiles.active}.properties")
public class CamundaConfig {

    @Value("${camunda.serviceUrl}")
    public String serviceUrl;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                .setConnectTimeout(10 * 1000)
                .setReadTimeout(10 * 1000)
                .build();
    }

}
