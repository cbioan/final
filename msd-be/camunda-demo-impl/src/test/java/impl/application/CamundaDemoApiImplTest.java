package impl.application;

import api.domain.ActiveTask;
import api.domain.CompleteTask;
import api.domain.StartProcess;
import api.exception.ProcessNotFoundException;
import api.exception.ProcessStartFailedException;
import api.exception.TaskNotFoundException;
import impl.config.CamundaExceptionHandler;
import impl.infrastructure.CamundaDemoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {CamundaDemoApiImpl.class, CamundaExceptionHandler.class})
@AutoConfigureMockMvc
@AutoConfigureWebMvc
@EnableAutoConfiguration
public class CamundaDemoApiImplTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SimpMessageSendingOperations messagingTemplate;

    @MockBean
    private CamundaDemoService camundaDemoService;

    @MockBean
    private RestTemplate restTemplate;

    @InjectMocks
    private CamundaDemoApiImpl camundaDemoApiImpl;

    private static final String URL_ENDPOINT = "/camunda";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void beginProcess_expectStartSuccess() throws Exception {

        StartProcess startProcessResponseMock = new StartProcess("", "");
        startProcessResponseMock.setProcessId("1234567890");
        startProcessResponseMock.setTaskId("0987654321");

        Mockito.when(camundaDemoService.camundaStartProcess()).thenReturn(startProcessResponseMock);

        mockMvc
                .perform(get(URL_ENDPOINT + "/start"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.processId").hasJsonPath())
                .andExpect(jsonPath("$.taskId").hasJsonPath())
                .andExpect(jsonPath("$.processId").value(is(not(isEmptyOrNullString()))))
                .andExpect(jsonPath("$.taskId").value(is(not(isEmptyOrNullString()))))
                .andReturn();
    }

    @Test
    public void beginProcess_expectStartFail() throws Exception {

        Mockito.when(camundaDemoService.camundaStartProcess())
                .thenThrow(new ProcessStartFailedException());

        mockMvc
                .perform(get(URL_ENDPOINT + "/start"))
                .andExpect(status().is(404))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
    }

    @Test
    public void productSelect_expectSuccess() throws Exception {

        ActiveTask activeTaskResponseMock = new ActiveTask("");
        activeTaskResponseMock.setActiveTaskId("1234567890");

        Mockito.when(camundaDemoService.camundaProductSelect(Mockito.anyString(), Mockito.anyString()))
                .thenReturn(activeTaskResponseMock);

        mockMvc
                .perform(post(URL_ENDPOINT + "/productSelect/anyTaskId")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("anyString"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.activeTaskId").hasJsonPath())
                .andExpect(jsonPath("$.activeTaskId").value(is(not(isEmptyOrNullString()))))
                .andReturn();

    }

    @Test
    public void productSelect_expectFail() throws Exception {

        Mockito.when(camundaDemoService.camundaProductSelect(Mockito.anyString(), Mockito.anyString()))
                .thenThrow(new ProcessNotFoundException());

        mockMvc
                .perform(post(URL_ENDPOINT + "/productSelect/anyTaskId")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content("anyString"))
                .andExpect(status().is(404))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

    }

    @Test
    public void confirmAction_expectSuccess() throws Exception {

        CompleteTask completeTaskResponseMock = new CompleteTask("", "", true);
        completeTaskResponseMock.setTaskId("1234567890");
        completeTaskResponseMock.setMessage("Any message");

        Mockito.when(camundaDemoService.camundaCompleteTask(Mockito.anyString(), Mockito.anyBoolean()))
                .thenReturn(completeTaskResponseMock);

        mockMvc
                .perform(post(URL_ENDPOINT + "/complete/anyTaskId/true")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.message").hasJsonPath())
                .andExpect(jsonPath("$.taskId").hasJsonPath())
                .andExpect(jsonPath("$.message").value(is(not(isEmptyOrNullString()))))
                .andExpect(jsonPath("$.taskId").value(is(not(isEmptyOrNullString()))))
                .andReturn();

    }

    @Test
    public void confirmAction_expectFail() throws Exception {

        Mockito.when(camundaDemoService.camundaCompleteTask(Mockito.anyString(), Mockito.anyBoolean()))
                .thenThrow(new TaskNotFoundException());

        mockMvc
                .perform(post(URL_ENDPOINT + "/complete/anyTaskId/true")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(404))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();

    }

    @Test
    public void timeoutDelegate_expectSuccess() throws Exception {

        mockMvc
                .perform(post(URL_ENDPOINT + "/timeout/123456")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(200))
                .andReturn();

    }

    @Test
    public void notFoundDelegate_expectSuccess() throws Exception {

        mockMvc
                .perform(post(URL_ENDPOINT + "/notfound/123456")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(200))
                .andReturn();

    }

    @Test
    public void saveDelegate_expectSuccess() throws Exception {

        mockMvc
                .perform(post(URL_ENDPOINT + "/service/123456")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(status().is(200))
                .andReturn();

    }
}