package impl.infrastructure;

import api.domain.ActiveTask;
import api.domain.CompleteTask;
import api.domain.StartProcess;
import api.exception.ProcessStartFailedException;
import api.exception.TaskNotFoundException;
import impl.config.CamundaConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class CamundaDemoServiceTest {

    @Mock
    private CamundaConfig camundaConfig;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private CamundaDemoService camundaDemoService;

    private static final String MOCK_TASK_ID = "1234567890";
    private static final String MOCK_PROCESS_ID = "1234567890";
    private static final String MOCK_MESSAGE = "message";
    private static final String MOCK_BODY = "internet";
    private static final Boolean MOCK_ASYNC_MODE = true;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        camundaConfig.serviceUrl = "http://localhost";
    }

    @Test
    public void camundaStartProcess_expectSuccess() {
        StartProcess mockResponseBody = new StartProcess(MOCK_PROCESS_ID, MOCK_TASK_ID);
        this.restTemplateMock_withResult(StartProcess.class, mockResponseBody, HttpMethod.GET);
        StartProcess response = camundaDemoService.camundaStartProcess();
        Assert.assertNotNull(response);
        Assert.assertFalse(response.getProcessId().isEmpty());
        Assert.assertFalse(response.getTaskId().isEmpty());
    }

    @Test(expected = ProcessStartFailedException.class)
    public void camundaStartProcess_expectFail_NullPointerException() {
        this.restTemplateMock_NullResult(StartProcess.class, HttpMethod.GET);
        camundaDemoService.camundaStartProcess();
    }

    @Test(expected = ProcessStartFailedException.class)
    public void camundaStartProcess_expectFail_HttpException() {
        this.restTemplateMock_HttpException(StartProcess.class, HttpMethod.GET);
        camundaDemoService.camundaStartProcess();
    }

    @Test
    public void camundaProductSelect_expectSuccess() {
        ActiveTask mockResponseBody = new ActiveTask(MOCK_TASK_ID);
        this.mockProductSelect(mockResponseBody);
        ActiveTask response = this.camundaProductSelect();
        Assert.assertNotNull(response);
        Assert.assertFalse(response.getActiveTaskId().isEmpty());
    }

    @Test(expected = TaskNotFoundException.class)
    public void camundaProductSelect_expectFail_NullPointerException() {
        this.mockNullResult(ActiveTask.class, HttpMethod.POST);
        this.camundaProductSelect();
    }

    @Test(expected = TaskNotFoundException.class)
    public void camundaProductSelect_expectFail_HttpException() {
        this.mockHttpException(ActiveTask.class, HttpMethod.POST);
        this.camundaProductSelect();
    }

    @Test
    public void camundaCompleteTask_expectSuccess() {
        CompleteTask mockResponseBody = new CompleteTask(MOCK_TASK_ID, MOCK_MESSAGE, MOCK_ASYNC_MODE);
        this.mockCompleteTask(mockResponseBody);
        CompleteTask response = camundaDemoService.camundaCompleteTask(MOCK_TASK_ID, MOCK_ASYNC_MODE);
        Assert.assertNotNull(response);
        Assert.assertFalse(response.getMessage().isEmpty());
        Assert.assertFalse(response.getTaskId().isEmpty());
    }

    @Test(expected = TaskNotFoundException.class)
    public void camundaCompleteTask_expectFail_NullPointerException() {
        this.mockNullResult(CompleteTask.class, HttpMethod.POST);
        this.camundaCompleteTask();
    }

    @Test(expected = TaskNotFoundException.class)
    public void camundaCompleteTask_expectFail_HttpException() {
        this.mockHttpException(CompleteTask.class, HttpMethod.POST);
        this.camundaCompleteTask();
    }

    private <R> void restTemplateMock_withResult(Class<R> clazz, R mockResponseBody, HttpMethod method) {
        ResponseEntity<R> mockResult = new ResponseEntity<>(mockResponseBody, null, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(
                Mockito.anyString(),
                Mockito.eq(method),
                Mockito.any(HttpEntity.class),
                Mockito.eq(clazz))).thenReturn(mockResult);
    }

    private <R> void restTemplateMock_NullResult(Class<R> clazz, HttpMethod method) {
        Mockito.when(restTemplate.exchange(
                Mockito.anyString(),
                Mockito.eq(method),
                Mockito.any(HttpEntity.class),
                Mockito.eq(clazz))).thenReturn(null);
    }

    private <R> void restTemplateMock_HttpException(Class<R> clazz, HttpMethod method) {
        Mockito.when(restTemplate.exchange(
                Mockito.anyString(),
                Mockito.eq(method),
                Mockito.any(HttpEntity.class),
                Mockito.eq(clazz))).thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));
    }

    private void mockProductSelect(ActiveTask mockResponseBody) {
        this.restTemplateMock_withResult(ActiveTask.class, mockResponseBody, HttpMethod.POST);
    }

    private void mockCompleteTask(CompleteTask mockResponseBody) {
        this.restTemplateMock_withResult(CompleteTask.class, mockResponseBody, HttpMethod.POST);
    }

    private ActiveTask camundaProductSelect() {
        return camundaDemoService.camundaProductSelect(MOCK_TASK_ID, MOCK_BODY);
    }

    private void camundaCompleteTask() {
        camundaDemoService.camundaCompleteTask(MOCK_TASK_ID, MOCK_ASYNC_MODE);
    }

    private <R> void mockNullResult(Class<R> clazz, HttpMethod method) {
        this.restTemplateMock_NullResult(clazz, method);
    }

    private <R> void mockHttpException(Class<R> clazz, HttpMethod method) {
        this.restTemplateMock_HttpException(clazz, method);
    }
}