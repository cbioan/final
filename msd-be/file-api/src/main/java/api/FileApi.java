package api;

import api.domain.request.FileScope;
import api.domain.response.FileUploadResponseDto;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/upload")
public interface FileApi {
    @PostMapping
    FileUploadResponseDto fileUpload(@RequestParam("file") MultipartFile file, @RequestParam("scope") FileScope scope);
}
