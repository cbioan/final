package api.exception;

public class BadFileTypeException extends Exception {
    private static final String BAD_FILE = "Incorrect file type.";
    public BadFileTypeException(String m) {
        super(m);
    }
    public BadFileTypeException() {
        super(BAD_FILE);
    }
}
