package api.exception;

public class InvalidFileTypeException extends RuntimeException {
    private static final String INVALID_FILE_TYPE = "Uploaded file is of invalid type.";
    public InvalidFileTypeException(String m) {
        super(m);
    }
    public InvalidFileTypeException() {
        super(INVALID_FILE_TYPE);
    }
}
