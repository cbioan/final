package api.exception;

public class StorageException extends RuntimeException {
    private static final String STORAGE_FAILED = "File storage failed.";
    public StorageException(String m) {
        super(m);
    }
    public StorageException() {
        super(STORAGE_FAILED);
    }
}
