package api.domain.response;

import api.domain.request.FileScope;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileUploadResponseDto {

    @JsonProperty("original_name")
    private String originalName;
    @JsonProperty("processed_name")
    private String processedName;
    private FileScope scope;
    private Long size;
    @JsonProperty("mime_type")
    private String mimeType;

}
