package api.domain.request;

import javax.validation.constraints.NotNull;

@NotNull
public enum FileScope {
    LOCATION_ADD,
}
