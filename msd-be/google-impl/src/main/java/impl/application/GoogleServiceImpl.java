package impl.application;

import api.GoogleService;
import api.domain.GoogleLocationDto;
import impl.config.GoogleServiceConfig;
import impl.domain.GoogleResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Slf4j
@Component
public class GoogleServiceImpl implements GoogleService {

    @Autowired
    private GoogleServiceConfig googleServiceConfig;
    @Autowired
    private RestTemplate restTemplate;

    public GoogleLocationDto geoCodeAddress(String address) {

        try {
            ResponseEntity<GoogleResponse> response = performRequest(address);
            if (response != null) {
                return response.getBody().getResults().get(0).getGeometry().getLocation();
            } else {
                return returnNoResult();
            }
        } catch (IndexOutOfBoundsException | UnsupportedEncodingException | RestClientResponseException e) {
            log.error(e.getMessage());
            return returnNoResult();
        }
    }

    private GoogleLocationDto returnNoResult() {
        return new GoogleLocationDto(0.0, 0.0);
    }

    private ResponseEntity<GoogleResponse> performRequest(String address) throws UnsupportedEncodingException {
        String buildUrlParams = String.format("/json?address=%s&key=%s", URLEncoder.encode(address, "UTF-8"), googleServiceConfig.apiKey);
        return this.restTemplate.getForEntity(googleServiceConfig.geoCodeEndpoint + buildUrlParams, GoogleResponse.class);
    }

}
