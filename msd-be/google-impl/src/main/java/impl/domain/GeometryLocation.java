package impl.domain;

import api.domain.GoogleLocationDto;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GeometryLocation {

    private GoogleLocationDto location;

}
