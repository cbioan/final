package impl.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GoogleResultComponents {

    @JsonProperty(value = "formatted_address")
    private String formattedAddress;
    private GeometryLocation geometry;

}
