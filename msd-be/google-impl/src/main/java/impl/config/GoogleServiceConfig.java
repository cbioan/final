package impl.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

@Configuration
@PropertySource("classpath:google-${spring.profiles.active}.properties")
public class GoogleServiceConfig {

    @Value("${google.apiKey}")
    public String apiKey;
    @Value("${google.geoCodeUrlEndpoint}")
    public String geoCodeEndpoint;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder
                .setConnectTimeout(10 * 1000)
                .setReadTimeout(10 * 1000)
                .build();
    }
}
