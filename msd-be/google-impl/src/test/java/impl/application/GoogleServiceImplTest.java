package impl.application;

import api.domain.GoogleLocationDto;
import impl.config.GoogleServiceConfig;
import impl.domain.GeometryLocation;
import impl.domain.GoogleResponse;
import impl.domain.GoogleResultComponents;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class GoogleServiceImplTest {

    @Mock
    private RestTemplate restTemplate;
    @Mock
    private GoogleServiceConfig googleServiceConfig;
    private static final double DELTA = 0.001;

    @InjectMocks
    private GoogleServiceImpl google;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.googleServiceConfig.apiKey = "1234567890";
        this.googleServiceConfig.geoCodeEndpoint = "http://google.com";
    }

    @Test
    public void geoCodeAddress_expectSuccess() {

        GoogleLocationDto responseMock = new GoogleLocationDto(123.456, 46.2345);

        GoogleResponse mockResponseResult = new GoogleResponse();
        ArrayList<GoogleResultComponents> mockArray = new ArrayList<>();
        GoogleResultComponents mockResultComponent = new GoogleResultComponents();
        GeometryLocation mockGeometry = new GeometryLocation();

        mockGeometry.setLocation(responseMock);
        mockResultComponent.setGeometry(mockGeometry);
        mockArray.add(mockResultComponent);
        mockResponseResult.setResults(mockArray);

        ResponseEntity<GoogleResponse> mockResult = new ResponseEntity<>(
                mockResponseResult, null, HttpStatus.OK);

        when(restTemplate.getForEntity(
                Mockito.anyString(),
                Mockito.eq(GoogleResponse.class))).thenReturn(mockResult);

        GoogleLocationDto response = google.geoCodeAddress("Any string");

        Assert.assertNotNull(response);

    }

    @Test
    public void geoCodeAddress_expectFail_NullResult() {

        ResponseEntity<GoogleResponse> mockResult = null;
        when(restTemplate.getForEntity(
                Mockito.anyString(),
                Mockito.eq(GoogleResponse.class))).thenReturn(mockResult);
        assertExpectFail();

    }

    @Test
    public void geoCodeAddress_expectFail_Exception() {

        RestClientResponseException mockException =
                new RestClientResponseException("", 401, "", null, null, null);

        when(restTemplate.getForEntity(
                Mockito.anyString(),
                Mockito.eq(GoogleResponse.class))).thenThrow(mockException);
        assertExpectFail();

    }

    private void assertExpectFail() {
        GoogleLocationDto response = google.geoCodeAddress("Any string");
        Assert.assertEquals(0, response.getLat(), DELTA);
        Assert.assertEquals(0, response.getLng(), DELTA);
    }
}