package api;

import api.domain.request.LocationRequestDto;
import api.domain.response.LocationDto;
import api.domain.response.LocationsCountDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/location")
@RestController
public interface LocationApi {

    @PostMapping
    List<LocationDto> getLocations(@RequestBody LocationRequestDto requestBody);

    @PostMapping(value = "/count")
    LocationsCountDto getLocationsCount(@RequestBody LocationRequestDto requestBody);
}
