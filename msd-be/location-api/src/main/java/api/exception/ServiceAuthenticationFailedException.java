package api.exception;

public class ServiceAuthenticationFailedException extends RuntimeException {
    private static final String FAILED_SERVICE_AUTHENTICATION = "Could not authenticate with service provider.";

    public ServiceAuthenticationFailedException(String m) {
        super(m);
    }

    public ServiceAuthenticationFailedException() {
        super(FAILED_SERVICE_AUTHENTICATION);
    }
}
