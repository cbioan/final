package api.exception;

public class LocationNotFoundException extends RuntimeException {
    private static final String LOCATIONS_NOT_FOUND = "Locations not found.";

    public LocationNotFoundException(String m) {
        super(m);
    }

    public LocationNotFoundException() {
        super(LOCATIONS_NOT_FOUND);
    }
}
