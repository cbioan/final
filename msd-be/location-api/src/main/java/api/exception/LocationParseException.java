package api.exception;

public class LocationParseException extends RuntimeException {
    private static final String LOCATIONS_UNEXPECTED_ERROR = "Un unexpected error occured while searching for locations.";

    public LocationParseException(String m) {
        super(m);
    }

    public LocationParseException() {
        super(LOCATIONS_UNEXPECTED_ERROR);
    }
}
