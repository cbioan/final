package api.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LocationsCountDto {

    @JsonProperty(value = "servicecount")
    private Integer serviceCount;
}
