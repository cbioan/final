package api.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LocationDto {
    private String address;
    private String country;
    @JsonProperty(value = "postal_code")
    private String postalCode;
    private String street;
    @JsonProperty(value = "house_number")
    private String houseNumber;
    private String state;
    @JsonProperty(value = "location_name")
    private String locationName;
    @JsonProperty(value = "tenantid")
    private String tenantId;
    @JsonProperty(value = "servicename")
    private String serviceName;
    private String uuid;
    @JsonProperty(value = "status")
    private String status;
    @NotNull
    private Double longitude = 0.0;
    @NotNull
    private Double latitude = 0.0;
}
