package api.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class LocationRequestDto {

    public enum Statuses {
        @JsonProperty("linked")
        LINKED {
            @Override
            public String toString() {
                return "linked";
            }
        },
        @JsonProperty("unlinked")
        UNLINKED {
            @Override
            public String toString() {
                return "unlinked";
            }
        },
        @JsonProperty("in_progress")
        INPROGRESS {
            @Override
            public String toString() {
                return "in_progress";
            }
        }
    }

    @NotNull
    private String customerId;
    @NotNull
    private Statuses status = Statuses.LINKED;
    @JsonProperty(value = "searchBranch")
    private String searchPhrase;
    private Boolean descending = false;
    private Integer page = 0;
    @JsonProperty(value = "perPage")
    private Integer resultsPerPage = 0;

}
