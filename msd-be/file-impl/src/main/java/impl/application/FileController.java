package impl.application;

import api.FileApi;
import api.domain.request.FileScope;
import api.domain.response.FileUploadResponseDto;
import impl.domain.FileProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileController implements FileApi {

    @Autowired
    FileProcessService fileProcessService;

    @PostMapping
    public FileUploadResponseDto fileUpload(@RequestParam("file") MultipartFile file, @RequestParam(value = "scope") FileScope scope){
        return fileProcessService.processFile(file,scope);
    }

}
