package impl.domain;

import api.domain.response.FileUploadResponseDto;
import api.exception.StorageException;
import impl.config.StorageConfig;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;

@Service
public class StorageServiceImpl implements StorageService {

    private final Path rootLocation;

    public StorageServiceImpl(StorageConfig config) {
        this.rootLocation = Paths.get(config.rootPath);
    }

    public FileUploadResponseDto store(MultipartFile file) {
        this.initPath();
        try (InputStream inputStream = file.getInputStream()) {
            String filename = StringUtils.cleanPath(file.getOriginalFilename());
            String processedName = generateProcessedName(filename);
            Files.copy(inputStream, this.rootLocation.resolve(processedName), StandardCopyOption.REPLACE_EXISTING);
            return createFileResponseDto(filename, processedName);
        } catch (IOException | NoSuchAlgorithmException e) {
            throw new StorageException(e.getMessage());
        }
    }

    private FileUploadResponseDto createFileResponseDto(String filename, String processedName) {
        FileUploadResponseDto result = new FileUploadResponseDto();
        result.setOriginalName(filename);
        result.setProcessedName(processedName);
        return result;
    }

    private String generateProcessedName(String originalName) throws NoSuchAlgorithmException {
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
        String rawName = timeStamp + originalName;
        byte[] bytesOfMessage = rawName.getBytes(StandardCharsets.UTF_8);
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] messageDigest = md.digest(bytesOfMessage);
        BigInteger numberRepresentation = new BigInteger(1, messageDigest);
        return numberRepresentation.toString(16) + "." + FilenameUtils.getExtension(originalName);
    }

    private void initPath() {
        try {
            if (!this.rootLocation.toFile().exists()) {
                Files.createDirectories(rootLocation);
            }
        } catch (IOException e) {
            throw new StorageException(e.getMessage());
        }
    }
}
