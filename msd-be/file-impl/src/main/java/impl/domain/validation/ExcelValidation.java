package impl.domain.validation;

import api.exception.BadFileTypeException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ExcelValidation extends Validation {

    private static final String[] ALLOWED_MIME = {
            "application/vnd.ms-excel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.template",
            "application/vnd.ms-excel.sheet.macroEnabled.12",
            "application/vnd.ms-excel.template.macroEnabled.12",
            "application/vnd.ms-excel.addin.macroEnabled.12",
            "application/vnd.ms-excel.sheet.binary.macroEnabled.12",
            "application/x-tika-msoffice"
    };

    public void validate(String mimeType) throws BadFileTypeException {
        log.info("Detected file type: " + mimeType);
        if(!this.validate(mimeType, ExcelValidation.ALLOWED_MIME)){
            throw new BadFileTypeException("Uploaded file is not a valid Excel file: " + mimeType);
        }
    }
}
