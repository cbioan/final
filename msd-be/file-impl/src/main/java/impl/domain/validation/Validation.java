package impl.domain.validation;

import api.exception.BadFileTypeException;

import java.util.Arrays;
import java.util.List;

public abstract class Validation {

    public abstract void validate(String mimeType) throws BadFileTypeException;

    protected Boolean validate(String validateMimeType, String[] against){
        List<String> list = Arrays.asList(against);
        return list.contains(validateMimeType);
    }
}
