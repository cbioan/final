package impl.domain;

import api.domain.response.FileUploadResponseDto;
import org.springframework.web.multipart.MultipartFile;

public interface StorageService {
    FileUploadResponseDto store(MultipartFile file);
}
