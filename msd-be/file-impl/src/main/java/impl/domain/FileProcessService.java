package impl.domain;

import api.domain.request.FileScope;
import api.domain.response.FileUploadResponseDto;
import api.exception.BadFileTypeException;
import api.exception.InvalidFileTypeException;
import api.exception.StorageException;
import impl.domain.validation.ExcelValidation;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

@Service
public class FileProcessService {

    @Autowired
    private StorageServiceImpl storageService;
    @Autowired
    private ExcelValidation excelValidation;

    private MultipartFile file;
    private FileScope fileScope;

    public FileUploadResponseDto processFile(MultipartFile file, FileScope scope) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        this.file = file;
        this.fileScope = scope;
        this.validateFile(file, filename);
        this.validateFileType();
        return this.saveFile();
    }

    private void validateFile(MultipartFile file, String filename) {
        if (file.isEmpty()) {
            throw new StorageException("Failed to store empty file " + filename);
        }
        if (filename.contains("..")) {
            throw new StorageException("Cannot store file with relative path outside current directory " + filename);
        }
    }

    private void validateFileType() {
        try {
            if(this.fileScope == FileScope.LOCATION_ADD){
                excelValidation.validate(this.getFileMimeType());
            } else {
                throw new StorageException("File type not allowed");
            }
        } catch (BadFileTypeException e) {
            throw new InvalidFileTypeException(e.getMessage());
        }
    }

    private FileUploadResponseDto saveFile(){
        FileUploadResponseDto result = storageService.store(this.file);
        result.setScope(fileScope);
        result.setMimeType(this.getFileMimeType());
        result.setSize(this.file.getSize());
        return result;
    }


    private String getFileMimeType() {
        try(InputStream inputStream = new BufferedInputStream(this.file.getInputStream())) {
            Tika fileInfo = new Tika();
            return fileInfo.detect(inputStream);
        } catch (Exception e) {
            throw new StorageException(e.getMessage());
        }
    }

}
