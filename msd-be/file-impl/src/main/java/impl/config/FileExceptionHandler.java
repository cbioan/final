package impl.config;

import api.exception.InvalidFileTypeException;
import api.exception.StorageException;
import common.error.ApiError;
import common.error.RestExceptionHandler;
import impl.application.FileController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.*;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

@RestControllerAdvice(assignableTypes = FileController.class)
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class FileExceptionHandler extends RestExceptionHandler {

    private ResponseEntity<Object> setBadRequest(String message) {
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
        apiError.setMessage(message);
        log.error(message);
        return buildResponseEntity(apiError);
    }

    private ResponseEntity<Object> setServerError(String message) {
        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR);
        apiError.setMessage(message);
        log.error(message);
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(MultipartException.class)
    protected ResponseEntity<Object> handleCustomException(MultipartException ex) {
        log.error(ex.getMessage());
        return setBadRequest(ex.getMessage());
    }

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    protected ResponseEntity<Object> handleCustomException(MaxUploadSizeExceededException ex) {
        log.error(ex.getMessage());
        return setBadRequest(ex.getMessage());
    }

    @ExceptionHandler(StorageException.class)
    protected ResponseEntity<Object> handleCustomException(StorageException ex) {
        log.error(ex.getMessage());
        return setServerError(ex.getMessage());
    }

    @ExceptionHandler(InvalidFileTypeException.class)
    protected ResponseEntity<Object> handleCustomException(InvalidFileTypeException ex) {
        log.error(ex.getMessage());
        return setBadRequest(ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(
            TypeMismatchException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        log.error(ex.getMessage());
        return setBadRequest(ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(
            MissingServletRequestPartException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        log.error(ex.getMessage());
        return setBadRequest(ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        log.error(ex.getMessage());
        return setBadRequest(ex.getMessage());
    }
}



