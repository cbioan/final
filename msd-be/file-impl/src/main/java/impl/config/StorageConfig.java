package impl.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:storage-${spring.profiles.active}.properties")
@Configuration
public class StorageConfig {

    @Value("${storage.rootPath}")
    public String rootPath;
}
