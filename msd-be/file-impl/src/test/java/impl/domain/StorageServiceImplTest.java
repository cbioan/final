package impl.domain;

import api.domain.request.FileScope;
import api.domain.response.FileUploadResponseDto;
import impl.config.StorageConfig;
import impl.domain.validation.ExcelValidation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class StorageServiceImplTest {

    @Mock
    StorageConfig config;

    private StorageServiceImpl storageServiceImpl;

    private static final String FILE_CONTENT = "AnyStringContent";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        config.rootPath = "/";
        storageServiceImpl = new StorageServiceImpl(config);
    }

    @Test
    public void store_expect_Success() {
//        MockMultipartFile mockFile = this.createMultipartFileMock();
//        FileUploadResponseDto response = storageServiceImpl.store(mockFile);
//        Assert.assertNotNull(response);

    }

    private MockMultipartFile createMultipartFileMock() {
        return new MockMultipartFile("file", "test.xls",
                "text/plain", FILE_CONTENT.getBytes());
    }

    private FileUploadResponseDto createFileResponseMock() {
        FileUploadResponseDto mockFileResponse = new FileUploadResponseDto();
        mockFileResponse.setOriginalName("test.xls");
        mockFileResponse.setProcessedName("123456.xls");
        mockFileResponse.setMimeType("mimeType");
        mockFileResponse.setSize(1234L);
        mockFileResponse.setScope(FileScope.LOCATION_ADD);
        return mockFileResponse;
    }

}