package impl.domain;

import api.domain.request.FileScope;
import api.domain.response.FileUploadResponseDto;
import api.exception.BadFileTypeException;
import api.exception.InvalidFileTypeException;
import api.exception.StorageException;
import impl.domain.validation.ExcelValidation;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class FileProcessServiceTest {

    @Mock
    StorageServiceImpl storageService;
    @Spy
    ExcelValidation excelValidation;

    @InjectMocks
    private FileProcessService fileProcessService;

    private static final String FILE_CONTENT = "AnyStringContent";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void processFile_expect_Success() throws BadFileTypeException {
        MockMultipartFile multipartFile = createMultipartFileMock();
        FileUploadResponseDto mockFileResponse = createFileResponseMock();

        doNothing().when(excelValidation).validate(Mockito.anyString());
        verify(excelValidation, never()).validate(Mockito.anyString());

        when(storageService.store(Mockito.any(MultipartFile.class)))
                .thenReturn(mockFileResponse);

        FileUploadResponseDto response = fileProcessService.processFile(multipartFile, FileScope.LOCATION_ADD);

        Assert.assertNotNull(response);
    }

    @Test(expected = InvalidFileTypeException.class)
    public void processFile_expect_Fail_Bad_File() {
        MockMultipartFile multipartFile = createMultipartFileMock();
        fileProcessService.processFile(multipartFile, FileScope.LOCATION_ADD);
    }

    @Test(expected = StorageException.class)
    public void processFile_expect_Fail_Bad_Scope() {
        MockMultipartFile multipartFile = createMultipartFileMock();
        fileProcessService.processFile(multipartFile, null);
    }

    private MockMultipartFile createMultipartFileMock() {
        return new MockMultipartFile("file", "test.xls",
                "text/plain", FILE_CONTENT.getBytes());
    }

    private FileUploadResponseDto createFileResponseMock() {
        FileUploadResponseDto mockFileResponse = new FileUploadResponseDto();
        mockFileResponse.setOriginalName("test.xls");
        mockFileResponse.setProcessedName("123456.xls");
        return mockFileResponse;
    }
}