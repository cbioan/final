package impl.application;

import api.domain.request.FileScope;
import api.domain.response.FileUploadResponseDto;
import impl.config.FileExceptionHandler;
import impl.config.StorageConfig;
import impl.domain.FileProcessService;
import impl.domain.StorageServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = {FileController.class, FileExceptionHandler.class})
@AutoConfigureMockMvc
@AutoConfigureWebMvc
@EnableAutoConfiguration
public class FileControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    FileProcessService fileProcessService;

    @InjectMocks
    FileController fileController;

    private static final String FILE_CONTENT = "AnyStringContent";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void fileUpload_expect_Success() throws Exception {

        MockMultipartFile multipartFile = createMultipartFileMock();
        FileUploadResponseDto mockFileResponse = createFileResponseMock();

        when(fileProcessService.processFile(Mockito.any(MultipartFile.class), Mockito.any(FileScope.class)))
                .thenReturn(mockFileResponse);

        mockMvc
                .perform(MockMvcRequestBuilders.multipart("/upload")
                        .file(multipartFile)
                .param("scope", "LOCATION_ADD"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.scope").hasJsonPath())
                .andExpect(jsonPath("$.size").hasJsonPath())
                .andExpect(jsonPath("$.original_name").hasJsonPath())
                .andExpect(jsonPath("$.processed_name").hasJsonPath())
                .andExpect(jsonPath("$.mime_type").hasJsonPath())
                .andReturn();

    }

    @Test
    public void fileUpload_expect_Fail_BadScope() throws Exception {

        MockMultipartFile multipartFile = createMultipartFileMock();
        FileUploadResponseDto mockFileResponse = createFileResponseMock();

        when(fileProcessService.processFile(Mockito.any(MultipartFile.class), Mockito.any(FileScope.class)))
                .thenReturn(mockFileResponse);

        performMockMvc(multipartFile, "BAD_SCOPE");

    }

    private void performMockMvc(MockMultipartFile multipartFile, String scope) throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.multipart("/upload")
                        .file(multipartFile)
                        .param("scope", scope))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().is(400))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andReturn();
    }

    private MockMultipartFile createMultipartFileMock() {
        return new MockMultipartFile("file", "test.xls",
                "application/x-tika-msoffice", FILE_CONTENT.getBytes());
    }

    private FileUploadResponseDto createFileResponseMock() {
        FileUploadResponseDto mockFileResponse = new FileUploadResponseDto();
        mockFileResponse.setSize(12345L);
        mockFileResponse.setMimeType("application/x-tika-msoffice");
        mockFileResponse.setOriginalName("test.xls");
        mockFileResponse.setProcessedName("123456.xls");
        return mockFileResponse;
    }
}