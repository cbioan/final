package impl.application;

import api.ServiceDirector;
import impl.infrastructure.ServiceDirectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceDirectorImpl implements ServiceDirector {

    @Autowired
    private ServiceDirectorRepository serviceDirectorRepository;

    public <R> R getLocations(String url, String body, Class<R> returnType) {
        return this.serviceDirectorRepository.getBranches(url, body, returnType);
    }

}
