package impl.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:hpesd-${spring.profiles.active}.properties")
@Configuration
public class HpeConfig {

    @Value("${hpe.username}")
    public String username;
    @Value("${hpe.password}")
    public String password;
    @Value("${hpe.serviceUrl}")
    public String serviceUrl;
    @Value("${hpe.serviceAuthTenantName}")
    public String tenantName;

}
