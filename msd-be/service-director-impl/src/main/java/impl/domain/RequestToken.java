package impl.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestToken {
    private Auth auth;
}
