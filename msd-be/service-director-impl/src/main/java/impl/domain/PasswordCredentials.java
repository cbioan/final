package impl.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PasswordCredentials {
    private String username;
    private String password;
}
