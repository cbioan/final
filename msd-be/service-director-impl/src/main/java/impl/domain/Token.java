package impl.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Token {
    private String id;
    private String expires;
    private String expiresAsDate;
}
