package impl.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Auth {
    private PasswordCredentials passwordCredentials;
    private String tenantName;
}
