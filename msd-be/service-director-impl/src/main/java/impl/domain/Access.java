package impl.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Access {
    private Token token;
}
