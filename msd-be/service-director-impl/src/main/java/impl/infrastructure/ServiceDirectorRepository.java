package impl.infrastructure;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import exception.SdAuthenticationFailedException;
import exception.SdProcessingException;
import exception.SdResultsNotFoundException;
import impl.config.HpeConfig;
import impl.domain.Auth;
import impl.domain.PasswordCredentials;
import impl.domain.RequestToken;
import impl.domain.RequestTokenResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;
import java.util.function.Supplier;

@Slf4j
@Repository
public class ServiceDirectorRepository {

    private HpeConfig config;
    private static final Integer MAX_ALLOWED_FAILED = 5;
    private HttpHeaders headers;
    private String serviceToken = null;
    private RestTemplate restTemplate;
    private Integer failedSignInAttempts;

    public ServiceDirectorRepository(HpeConfig config, RestTemplate restTemplate) {
        this.config = config;
        this.restTemplate = restTemplate;
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        this.failedSignInAttempts = 0;
    }

    public <R> R getBranches(String url, String body, Class<R> returnType) {

        if (this.failedSignInAttempts >= MAX_ALLOWED_FAILED) {
            throw new SdAuthenticationFailedException();
        }

        try {
            return performPostRequest(url, body, returnType);
        } catch (SdAuthenticationFailedException e) {
            log.error(e.getMessage());
            return performLambdaCall(url, body, returnType);
        } catch (RestClientResponseException e) {
            log.error(e.getMessage());
            if (performStatusCheck(e.getRawStatusCode())) {
                return performLambdaCall(url, body, returnType);
            } else throw new SdProcessingException(e.getMessage());
        } catch (Exception e) {
            throw new SdResultsNotFoundException(e.getMessage());
        }
    }

    private void authenticate() {
        try {
            this.performAuthRequest();
        } catch (Exception e) {
            log.error(e.getMessage());
            this.serviceToken = null;
            this.failedSignInAttempts++;
        }
    }

    private void performAuthRequest() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        RequestToken requestToken = new RequestToken();

        PasswordCredentials passwordCredentials = createPasswordBody();
        Auth requestWithCredentials = createRequestCredentials(passwordCredentials);
        requestToken.setAuth(requestWithCredentials);

        String callUrl = this.config.serviceUrl + "/v2/tokens";
        HttpEntity<String> entity = new HttpEntity<>(mapper.writeValueAsString(requestToken), headers);
        ResponseEntity<RequestTokenResponse> response = restTemplate.postForEntity(callUrl, entity, RequestTokenResponse.class);
        this.serviceToken = Objects.requireNonNull(response.getBody()).getAccess().getToken().getId();
    }

    private Auth createRequestCredentials(PasswordCredentials passwordCredentials) {
        Auth requestWithCredentials = new Auth();
        requestWithCredentials.setTenantName(this.config.tenantName);
        requestWithCredentials.setPasswordCredentials(passwordCredentials);
        return requestWithCredentials;
    }

    private PasswordCredentials createPasswordBody() {
        PasswordCredentials passwordCredentials = new PasswordCredentials();
        passwordCredentials.setPassword(this.config.password);
        passwordCredentials.setUsername(this.config.username);
        return passwordCredentials;
    }

    private <R> R authTokenProxy(Supplier<R> func) {
        this.authenticate();
        return func.get();
    }

    private boolean performStatusCheck(int statusCode) {
        return statusCode == 401;
    }

    private <R> R performPostRequest(String url, String body, Class<R> returnType) {
        headers.set("X-Auth-Token", this.serviceToken);
        ResponseEntity<R> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(body, headers), returnType);
        if (response.getStatusCode().value() == 401) {
            throw new SdAuthenticationFailedException();
        }
        return Objects.requireNonNull(response.getBody());
    }

    private <R> R performLambdaCall(String url, String body, Class<R> returnType) {
        return authTokenProxy(() -> getBranches(url, body, returnType));
    }
}
