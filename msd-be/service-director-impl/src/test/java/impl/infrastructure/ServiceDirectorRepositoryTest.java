package impl.infrastructure;

import exception.SdAuthenticationFailedException;
import exception.SdResultsNotFoundException;
import impl.config.HpeConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class ServiceDirectorRepositoryTest {

    @Mock
    private RestTemplate restTemplate;
    @Mock
    private HpeConfig config;

    @InjectMocks
    private ServiceDirectorRepository serviceDirectorRepository;

    private static final String RESPONSE_BODY = "This mocks the response body";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.config.password = "test";
        this.config.serviceUrl = "http://localhost";
        this.config.tenantName = "tenant";
        this.config.username = "test";
    }

    @Test
    public void getBranches_expectSuccess() {

        ResponseEntity<String> mockResult = new ResponseEntity<>(
                RESPONSE_BODY, null, HttpStatus.OK);

        when(restTemplate.exchange(
                Mockito.anyString(),
                Mockito.eq(HttpMethod.POST),
                Mockito.any(HttpEntity.class),
                Mockito.eq(String.class))).thenReturn(mockResult);

        String response = serviceDirectorRepository.getBranches("url", "body", String.class);
        Assert.assertNotNull(response);

    }

    @Test(expected = SdAuthenticationFailedException.class)
    public void getBranches_expectFail_Exception_Unauthorized() {

        ResponseEntity<String> mockResult = new ResponseEntity<>(
                RESPONSE_BODY, null, HttpStatus.UNAUTHORIZED);

        when(restTemplate.exchange(
                Mockito.anyString(),
                Mockito.eq(HttpMethod.POST),
                Mockito.any(HttpEntity.class),
                Mockito.eq(String.class))).thenReturn(mockResult);

        serviceDirectorRepository.getBranches("url", "body", String.class);

    }

    @Test(expected = SdAuthenticationFailedException.class)
    public void getBranches_expectFail_Exception_Unauthorized_throw() {

        RestClientResponseException mockException =
                new RestClientResponseException("", 401, "", null, null, null);

        when(restTemplate.exchange(
                Mockito.anyString(),
                Mockito.eq(HttpMethod.POST),
                Mockito.any(HttpEntity.class),
                Mockito.eq(String.class))).thenThrow(mockException);

        serviceDirectorRepository.getBranches("url", "body", String.class);

    }

    @Test(expected = SdResultsNotFoundException.class)
    public void getBranches_expectFail_Exception_NullPointer() {
        serviceDirectorRepository.getBranches("url", "body", Integer.class);
    }
}