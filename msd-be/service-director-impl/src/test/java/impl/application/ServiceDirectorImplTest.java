package impl.application;

import impl.infrastructure.ServiceDirectorRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.Matchers.instanceOf;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class ServiceDirectorImplTest {

    @Mock
    private ServiceDirectorRepository serviceDirectorRepository;

    @InjectMocks
    private ServiceDirectorImpl serviceDirectorImpl;

    private static final String MOCK_RESULT = "string";

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getLocations_expectSuccess() {

        when(serviceDirectorRepository.getBranches(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.eq(String.class))).thenReturn(MOCK_RESULT);

        String response = serviceDirectorImpl.getLocations("url", "body", String.class);

        Assert.assertNotNull(response);
        Assert.assertThat(response, instanceOf(String.class));

    }
}