package api;

import api.domain.GoogleLocationDto;

public interface GoogleService {

    GoogleLocationDto geoCodeAddress(String address);

}
