package websocket.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MsdMessage {

    private MessageType type;
    private String content;
    private String sender;

    public enum MessageType {
        TIMEOUT,
        NOT_FOUND,
        COMPLETE,
        COMM,
        JOIN,
        LEAVE
    }

}
